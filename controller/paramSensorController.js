const include = require('../routes/includeRouter/include');

const moment = include.moment;
const stringify = include.stringify;

const KwhSensor = require('../models/dataParamSensorModel');





 function setKwh(req, res, next) {

    try {
        let data = req.body;

        data.building_name = req.params.building_name;
        
     
        let kwhSensor = new KwhSensor(data);
        let errData = kwhSensor.validateSync();
        if (res.statusCode == 200) {
            if (!errData) {
    
                KwhSensor.find({ building_name: req.params.building_name }, function(err, kwhFinds) {
                    data.created_at = moment().format();
                    let kwhSensorSave = new KwhSensor(data);
                    if(kwhFinds.length < 1) {
                        kwhSensorSave.save(function(err, result) {
                            if (err) {
                                res.status(400).json({
                                    pesan: "Gagal mengisi",
                                });
                            } else {
                                res.status(200).json({
                                    pesan: "Berhasil mengisi Param ",
                                    data
                                });
                            }
                        });
                    }else {
                        KwhSensor.findOneAndUpdate({
           
                            building_name: req.params.building_name,
               
                        }, {
                            $set: {
                                cost : data.cost,
                                cost_limit : data.cost_limit,
                                volt: data.volt,
                                current: data.current,
                            }
                        },
        
                        function(err, result) {
        
                            res.status(200).json({
                                pesan: "Berhasil mengisi Param Update",
                                result
                            });
                        });
                    }
                    
    
                });
            } else {
                res.status(200).json({
                    pesan: "data error",
                });
            };
        } else {
    
            res.status(200).json({
                pesan: "Data Gedung TIdak tersedia",
            })
        }
    }catch(err) {
        res.status(404).json({
            pesan: "Error",
        })
    }
  

}

 function getKwh(req, res, next) {
    let data = req.body;

    data.building_name = req.params.building_name;
   

    let kwhSensor = new KwhSensor(data);
    let errData = kwhSensor.validateSync();

    var startDay = moment(req.params.start_date).startOf('day').format();
    var endDay = moment(req.params.end_date).endOf('day').format();
    if (res.statusCode == 200) {
        if (!errData) {
            KwhSensor.find({

                building_name: req.params.building_name,
 
            },  function(err, kwhFinds) {

                if (kwhFinds.length < 1) {
                    res.status(200).json({
                        pesan: "tidak ada data ",
                        data: kwhFinds,
                    });
                } else {

                    res.status(200).json({
                        pesan: "data Monitoring",
                        data: kwhFinds.map(data => {
                            return {
                                status: data.status,
                                cost : data.cost,
                                cost_limit : data.cost_limit,
                                volt: data.volt,
                                current: data.current,
                                created_at: data.created_at
                            }

                        })
                    });
                }
            });
        } else {
            res.status(200).json({
                pesan: "data error",
            });
        }
    } else {
        res.status(200).json({
            pesan: "Data Gedung Tidak tersedia",
        });
    }

}

function takeData(req, res, next) {
    let data = req.body;

    data.building_name = req.params.building_name;


    let kwhSensor = new KwhSensor(data);
    let errData = kwhSensor.validateSync();

    var startDay = moment(req.params.start_date).startOf('day').format();
    var endDay = moment(req.params.end_date).endOf('day').format();
    if (res.statusCode == 200) {
        if (!errData) {
            KwhSensor.find({

                building_name: req.params.building_name,
              
            },  function(err, kwhFinds) {

                if (kwhFinds.length < 1) {
                    
                    req.params.cost = 0;
                    res.statusCode = 404;
                    next()
                } else {
                    req.params.cost = kwhFinds[0].cost[0];
                    res.statusCode = 200;
                    next()
                    
                }
            });
        } else {
            res.status(200).json({
                pesan: "data error",
            });
        }
    } else {
        res.status(200).json({
            pesan: "Data Gedung Tidak tersedia",
        });
    }

}




exports.setKwh = setKwh;
exports.getKwh = getKwh;
exports.takeData = takeData;