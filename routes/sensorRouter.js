var express = require('express');
var router = express.Router();

const sensorController = require('../controller/sensorController');
const gedungController = require('../controller/gedungController');
const paramController = require('../controller/paramSensorController');
const auth = require('./../middleware/authentication')

//buat kirim data
router.post('/:building_name/:parent_name/:channel_name/:node_name', paramController.takeData, sensorController.setKwh, sensorController.setKWHHour, sensorController.setKWHDay, sensorController.setKWHWeek, sensorController.setKWHMonth, function(req, res, next) {});


router.get('/:building_name/:parent_name/:channel_name/:node_name/:start_date/:end_date', gedungController.checkGedung,  paramController.takeData,sensorController.getKwh, function(req, res, next) {});


router.get('/dayByGedung/:building_name/:start_date/:end_date',  gedungController.checkGedungOne,   sensorController.getKwhDayByGedung, function(req, res, next) {});

router.get('/monthByGedung/:building_name/:start_date/:end_date',  gedungController.checkGedungOne,   sensorController.getKwhMonthByGedung, function(req, res, next) {});

router.get('/hourByGedung/:building_name/:start_date/:end_date', gedungController.checkGedungOne, sensorController.getKwhHourByGedung, function(req, res, next) {});

router.get('/dayByDevice/:building_name/:node_name/:start_date/:end_date', gedungController.checkGedungOne, sensorController.getKwhDayByDevice, function(req, res, next) {});

router.get('/monthByDevice/:building_name/:node_name/:start_date/:end_date', gedungController.checkGedungOne, sensorController.getKwhMonthByDevice, function(req, res, next) {});

router.get('/hourByDevice/:building_name/:node_name/:start_date/:end_date', gedungController.checkGedungOne, sensorController.getKwhHourByDevice, function(req, res, next) {});

router.get('/hour/:building_name/:parent_name/:channel_name/:node_name/:start_date/:end_date', gedungController.checkGedung,  sensorController.getKwhHour, function(req, res, next) {});

router.get('/day/:building_name/:parent_name/:channel_name/:node_name/:start_date/:end_date', gedungController.checkGedung,  sensorController.getKwhDay, function(req, res, next) {});

router.get('/week/:building_name/:parent_name/:channel_name/:node_name/:start_date/:end_date', gedungController.checkGedung,  sensorController.getKwhWeek, function(req, res, next) {});

router.get('/month/:building_name/:parent_name/:channel_name/:node_name/:start_date/:end_date', gedungController.checkGedung,   sensorController.getKwhMonth, function(req, res, next) {});

module.exports = router;