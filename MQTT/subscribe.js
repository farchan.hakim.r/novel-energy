const mqtt = require('mqtt');
const client  = mqtt.connect('mqtt://localhost:1884');
const include = require('../routes/includeRouter/include');

const moment = include.moment;
const stringify = include.stringify;

const KwhSensor = require('../models/dataSensorModel');
const KwhHourSensor = require('../models/dataSensorHourModel');
const KwhDaySensor = require('../models/dataSensorDayModel');
const KwhWeekSensor = require('../models/dataSensorWeekModel');
const KwhMonthSensor = require('../models/dataSensorMonthModel');
const GedungModel = require('../models/dataGedungModel')

function checkGedung(data) {
    let kwhSensor = new GedungModel(data);
    let errData = kwhSensor.validateSync();
        

    if (!errData) {
        
        GedungModel.find({
            
            building_name: data.building_name,
            parent_name: data.parent_name,
            channel_name: data.channel_name,
            node_name: data.node_name
        }, function(err, kwhFinds) {
             
            if (kwhFinds.length < 1 ) {
                console.log('DATA GEDUNG TIDAK ADA')
             
            } else {
                console.log('DATA GEDUNG TERSEDIA')
                setKwh(data)
                // return  200;
            
            }
        });
    } else {
        console.log('DATA error')
    }
}

function setKwh(data) {
  
    try {
        let kwhSensor = new KwhSensor(data);
        let errData = kwhSensor.validateSync();
        let flagMinutes, flagHour, flagDay, flagWeeks, flagMonth, flagYear;
    
      
            if (!errData) {
                data.created_at = moment().format();
                let kwhSensorSave = new KwhSensor(data);
                kwhSensorSave.save(function(err, result) {
                    if (err) {
                        console.log('DATA ERROR')
                    } else {
                        console.log('BERHASIL MENGISI LIVE DATA')
                        setKWHMonth(data)
                    }
                    
                });
    
            } else {
                console.log('DATA ERROR')
            };        
    }catch(err) {
        console.log(err)
    }
   
}
function setKWHMonth(data) {

    try {
        var startMonth = moment().startOf('month').format();
    var endMonth = moment().endOf('month').format();

    KwhMonthSensor.find({
        'created_at': {
            $gte: startMonth,
            $lte: endMonth
        },
        building_name: data.building_name,
        parent_name: data.parent_name,
        channel_name: data.channel_name,
        node_name: data.node_name
    },  function(err, kwhFindsMonth) {
        if (kwhFindsMonth.length < 1) {
            let KwhMonthSensorData = new KwhMonthSensor(data);
            KwhMonthSensorData.save(function(err, result) {
                if (err) {
                    console.log('GAGAL MENGISI')
                } else {
                    console.log('BERHASIL MENGISI')
                    setKWHWeek(data)
                }
            });
        } else {


            let volt = [data.volt[0] , data.volt[1] , data.volt[2] ]
            let current = [data.current[0] , data.current[1],  data.current[2] ]
            let kwh = [data.kwh[0] + kwhFindsMonth[0].kwh[0], data.kwh[1] + kwhFindsMonth[0].kwh[1], data.kwh[2] + kwhFindsMonth[0].kwh[2]]
            KwhMonthSensor.findOneAndUpdate({
                    'created_at': {
                        $gte: startMonth,
                        $lte: endMonth
                    },
                    building_name: data.building_name,
                    parent_name: data.parent_name,
                    channel_name: data.channel_name,
                    node_name: data.node_name
                }, {
                    $set: {
                        'volt': volt,
                        'current': current,
                        'kwh': kwh,

                    }
                },

                function(err, result4) {
                    console.log('BERHASIL MEGISI MONTH DATA')
                    setKWHWeek(data)
                });
        }
    });

    }catch(err) {
        console.log(err)
    }
    

}

function setKWHWeek(data) {


    try {
        var startWeek = moment().startOf('week').format();
        var endWeek = moment().endOf('week').format();
    
        KwhWeekSensor.find({
            'created_at': {
                $gte: startWeek,
                $lte: endWeek
            },
            building_name: data.building_name,
            parent_name: data.parent_name,
            channel_name: data.channel_name,
            node_name: data.node_name
        },  function(err, kwhFindsWeek) {
            if (kwhFindsWeek.length < 1) {
                let KwhWeekSensorData = new KwhWeekSensor(data);
                KwhWeekSensorData.save(function(err, result) {
                    if (err) {
                        console.log('GAGAL MENGISI')
                    } else {
                        console.log('BERHASIL MENGISI DATA WEEK')
                        setKWHDay(data)
                    }
                });
            } else {
    
    
                let volt = [data.volt[0] , data.volt[1] , data.volt[2] ]
                let current = [data.current[0] , data.current[1],  data.current[2] ]
                let kwh = [data.kwh[0] + kwhFindsWeek[0].kwh[0], data.kwh[1] + kwhFindsWeek[0].kwh[1], data.kwh[2] + kwhFindsWeek[0].kwh[2]]
    
                KwhWeekSensor.findOneAndUpdate({
                        'created_at': {
                            $gte: startWeek,
                            $lte: endWeek
                        },
                        building_name: data.building_name,
                        parent_name: data.parent_name,
                        channel_name: data.channel_name,
                        node_name: data.node_name
                    }, {
                        $set: {
                            'volt': volt,
                            'current': current,
                            'kwh': kwh,
    
                        }
                    },
    
                    function(err, result4) {
                        console.log('BERHASIL MENGISI DATA WEEK')
                        setKWHDay(data)
                    });
            }
        });
    
    }catch(err) {
        console.log(err)
    }
  

}

function setKWHDay(data) {


    try {
        var startDay = moment().startOf('day').format();
    var endDay = moment().endOf('day').format();

    KwhDaySensor.find({
        'created_at': {
            $gte: startDay,
            $lte: endDay
        },
        building_name: data.building_name,
        parent_name: data.parent_name,
        channel_name: data.channel_name,
        node_name: data.node_name
    },  function(err, kwhFindsDay) {
        if (kwhFindsDay.length < 1) {
            let KwhDaySensorData = new KwhDaySensor(data);
            KwhDaySensorData.save(function(err, result) {
                if (err) {
                    console.log('GAGAL MENGISI')
                } else {
                    console.log('BERHASIL MENGISI DATA DAY')
                    setKWHHour(data)
                }
            });
        } else {


            let volt = [data.volt[0] , data.volt[1] , data.volt[2] ]
            let current = [data.current[0] , data.current[1],  data.current[2] ]
            let kwh = [data.kwh[0] + kwhFindsDay[0].kwh[0], data.kwh[1] + kwhFindsDay[0].kwh[1], data.kwh[2] + kwhFindsDay[0].kwh[2]]

            KwhDaySensor.findOneAndUpdate({
                    'created_at': {
                        $gte: startDay,
                        $lte: endDay
                    },
                    building_name: data.building_name,
                    parent_name: data.parent_name,
                    channel_name: data.channel_name,
                    node_name: data.node_name
                }, {
                    $set: {
                        'volt': volt,
                        'current': current,
                        'kwh': kwh,

                    }
                },

                function(err, result3) {

                    console.log('BERHASIL MENGISI DATA DAY')
                    setKWHHour(data)
                });
        }
    });

    }catch(err) {
        console.log(err)
    }

    

}

 function setKWHHour(data) {

    try {
        var startHour = moment().startOf('hour').format();
        var endHour = moment().endOf('hour').format();
    
        KwhHourSensor.find({
            'created_at': {
                $gte: startHour,
                $lte: endHour
            },
            building_name: data.building_name,
            parent_name: data.parent_name,
            channel_name: data.channel_name,
            node_name: data.node_name
        },   async function(err, kwhFindsHour) {
    
            if ( await kwhFindsHour.length < 1) {
                let KwhHourSensorData = new KwhHourSensor(data);
                KwhHourSensorData.save( function(err, result) {
                    if (err) {
                        console.log('GAGAL MENGISI')
                    } else {
                        console.log('BERHASIL MENGISI DATA HOUR')
                    }
                });
            } else {
                let volt =  [data.volt[0] + kwhFindsHour[0].volt[0], data.volt[1] + kwhFindsHour[0].volt[1], data.volt[2] + kwhFindsHour[0].volt[2]]
                let current = [data.current[0] + kwhFindsHour[0].current[0], data.current[1] + kwhFindsHour[0].current[1], data.current[2] + kwhFindsHour[0].current[2]]
                let kwh =  [data.kwh[0] + kwhFindsHour[0].kwh[0], data.kwh[1] + kwhFindsHour[0].kwh[1], data.kwh[2] + kwhFindsHour[0].kwh[2]]
    
    
                KwhHourSensor.findOneAndUpdate({
                        'created_at': {
                            $gte: startHour,
                            $lte: endHour
                        },
                        building_name: data.building_name,
                        parent_name: data.parent_name,
                        channel_name: data.channel_name,
                        node_name: data.node_name
                    }, {
                        $set: {
                            'volt': volt,
                            'current': current,
                            'kwh': kwh,
    
                        }
                    },
    
                    function(err, result2) {
                       console.log('BERHASIL MENGISI DATA HOUR')
                    });
            }
        });
    
    
    }catch(err) {
        console.log(err)
    }
   
}
let data;

client.on('connect', function () {
        client.subscribe('sendEnergy')
})
client.on('message', function (topic, message) {
    context = message.toString();
    data = JSON.parse(context);
   
    checkGedung(data)
    
    
})
