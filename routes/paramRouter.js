var express = require('express');
var router = express.Router();

const sensorController = require('../controller/paramSensorController');
const gedungController = require('../controller/gedungController');
const auth = require('./../middleware/authentication')

//buat kirim data
router.post('/:building_name', gedungController.checkGedungForParam, sensorController.setKwh, function(req, res, next) {});


router.get('/:building_name', gedungController.checkGedungForParam,  sensorController.getKwh, function(req, res, next) {});


module.exports = router;