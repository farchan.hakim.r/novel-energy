const include = require('../routes/includeRouter/include');

const mongoose = include.mongoose;
const moment = include.moment;

const sensorDaySchema = new mongoose.Schema({
    building_name: { type: String },
    parent_name: { type: String },
    channel_name: { type: String },
    node_name: { type: String },
    status: { type: Boolean, default: true },
    volt: [{ type: Number }],
    current: [{ type: Number }],
    kwh: [{ type: Number }],
    cost : [{type : Number, default : 0}],
    created_at: { type: String, default: moment().format() },
    created_at_ISO: { type: Date, default: new Date },


})

const SensorDayKwh = mongoose.model('SensorDayKwh', sensorDaySchema);

module.exports = SensorDayKwh;