const include = require('../routes/includeRouter/include');

const moment = include.moment;
const stringify = include.stringify;

const KwhSensor = require('../models/dataSensorModel');
const KwhHourSensor = require('../models/dataSensorHourModel');
const KwhDaySensor = require('../models/dataSensorDayModel');
const KwhWeekSensor = require('../models/dataSensorWeekModel');
const KwhMonthSensor = require('../models/dataSensorMonthModel');






 async function setKwh(req, res, next) {

    try {
        let data = req.body;
        // 
        dataHour = data;
    
        data.building_name = req.params.building_name;
        data.parent_name = req.params.parent_name;
        data.channel_name = req.params.channel_name;
        data.node_name = req.params.node_name;
        dataMonth = dataWeek = dataDay = dataHour = data;
     
        let kwhSensor = new KwhSensor(data);
        let errData = kwhSensor.validateSync();
        let flagMinutes, flagHour, flagDay, flagWeeks, flagMonth, flagYear;
        
        if(data.kwh.length == 1) {
            data.apparent_power = [data.volt[0] * data.current[0]],
            data.Q = [Math.sqrt(data.apparent_power*data.apparent_power - data.power[0]*data.power[0])]
            data.voltage_l_l = [0]
            data.power_3_phase = [0]
            data.reactive_power = [0]
            data.cost = [req.params.cost*data.kwh[0]]
        }else {
            data.voltage_l_l = [data.volt[0] * Math.sqrt(3), data.volt[1] * Math.sqrt(3), data.volt[2] * Math.sqrt(3)]
            data.apparent_power = [ (data.volt[0] *data.current[0])/1000,
                                    (data.volt[1] *data.current[1])/1000,
                                    (data.volt[2] *data.current[2])/1000
                                    ]
            data.Q = [  (Math.acos(data.cosphi[0])),
                        (Math.acos(data.cosphi[1])),
                        (Math.acos(data.cosphi[2]))
                        ]
            data.reactive_power = [   data.apparent_power[0]*Math.sin(Math.acos(data.cosphi[0])),
                            data.apparent_power[1]*Math.sin(Math.acos(data.cosphi[1])),
                            data.apparent_power[2]*Math.sin(Math.acos(data.cosphi[2]))  ]
            data.power_3_phase = [ 0,0,0]
            if(data.cosphi[0] == 0)  data.cosphi[0] = 0.1
            if(data.cosphi[1] == 0)  data.cosphi[1] = 0.1
            if(data.cosphi[2] == 0)  data.cosphi[2] = 0.1

            data.cost = [((data.kwh[0]/data.cosphi[0]) + (data.kwh[1]/data.cosphi[1]) + (data.kwh[2]/data.cosphi[2])) * req.params.cost]
        }

    
        if (res.statusCode == 200) {
            if (!errData) {
    
                KwhSensor.find({ building_name: req.params.building_name, parent_name: req.params.parent_name, channel_name: req.params.channel_name, node_name: req.params.node_name }, await function(err, kwhFinds) {
                    data.created_at = moment().format();
                    let kwhSensorSave = new KwhSensor(data);
                    kwhSensorSave.save( function(err, result) {
                        if (err) {
                            res.status(400)
                            console.log(err)
                        } else {
                            flagMinutes = true;
                        }
                        // console.log(result)
                    });
                    next()
    
    
                });
            } else {
                res.status(200).json({
                    pesan: "data error",
                });
            };
        } else {
    
            res.status(200).json({
                pesan: "Data Gedung TIdak tersedia",
            })
        }
    }catch(err) {
        console.log(err)
    }
  

}

 async function setKWHMonth(req, res, next) {

    try {
        data = req.body;
        data.building_name = req.params.building_name;
        data.parent_name = req.params.parent_name;
        data.channel_name = req.params.channel_name;
        data.node_name = req.params.node_name;
    
    
        var startMonth = moment().startOf('month').format();
        var endMonth = moment().endOf('month').format();
    
        KwhMonthSensor.find({
            'created_at': {
                $gte: startMonth,
                $lte: endMonth
            },
            building_name: req.params.building_name,
            parent_name: req.params.parent_name,
            channel_name: req.params.channel_name,
            node_name: req.params.node_name
        },  await function(err, kwhFindsMonth) {
            if (!kwhFindsMonth || kwhFindsMonth.length < 1) {
                let KwhMonthSensorData = new KwhMonthSensor(data);
                KwhMonthSensorData.save(function(err, result) {
                    if (err) {
                        res.status(400)
                    } else {
                        res.status(200).json({
                            pesan: "Berhasil mengisi",
                        });
                    }
                });
            } else {
    
    
                let volt = []
                let current = []
                let kwh = []
              
                if(data.kwh.length == 1) {
                    kwh = [data.kwh[0] + kwhFindsMonth[0].kwh[0]]
                    volt = [data.volt[0]  ]
                    current = [data.current[0] ]
                    
                }else {
                    volt = [data.volt[0] , data.volt[1] , data.volt[2] ]
                    current = [data.current[0] , data.current[1],  data.current[2] ]
                    kwh = [data.kwh[0] + kwhFindsMonth[0].kwh[0], data.kwh[1] + kwhFindsMonth[0].kwh[1], data.kwh[2] + kwhFindsMonth[0].kwh[2]]
                }
    
                KwhMonthSensor.findOneAndUpdate({
                        'created_at': {
                            $gte: startMonth,
                            $lte: endMonth
                        },
                        building_name: req.params.building_name,
                        parent_name: req.params.parent_name,
                        channel_name: req.params.channel_name,
                        node_name: req.params.node_name
                    }, {
                        $set: {
                            'volt': volt,
                            'current': current,
                            'kwh': kwh,
                            'cost' : data.cost
                        }
                    },
    
                     function(err, result) {
    
                        res.status(200).json({
                            pesan : "berhasil mengisi"

                            })
                    });
            }
        });
    
    }catch(err) {
        console.log(err)
    }
   

}

 async function setKWHWeek(req, res, next) {

    try {
        data = req.body;
        // console.log(data)
        data.building_name = req.params.building_name;
        data.parent_name = req.params.parent_name;
        data.channel_name = req.params.channel_name;
        data.node_name = req.params.node_name;
    
    
        var startWeek = moment().startOf('week').format();
        var endWeek = moment().endOf('week').format();
    
        KwhWeekSensor.find({
            'created_at': {
                $gte: startWeek,
                $lte: endWeek
            },
            building_name: req.params.building_name,
            parent_name: req.params.parent_name,
            channel_name: req.params.channel_name,
            node_name: req.params.node_name
        },  await function(err, kwhFindsWeek) {
            if (!kwhFindsWeek || kwhFindsWeek.length < 1) {
                let KwhWeekSensorData = new KwhWeekSensor(data);
                KwhWeekSensorData.save(function(err, result) {
                    if (err) {
                        res.status(400)
                    } else {
                        next()
                    }
                });
            } else {
    
    
                let volt = []
                let current = []
                let kwh = []
              
                if(data.kwh.length == 1) {
                    kwh = [data.kwh[0] + kwhFindsWeek[0].kwh[0]]
                    volt = [data.volt[0]  ]
                    current = [data.current[0] ]
                    
                }else {
                    volt = [data.volt[0] , data.volt[1] , data.volt[2] ]
                    current = [data.current[0] , data.current[1],  data.current[2] ]
                    kwh = [data.kwh[0] + kwhFindsWeek[0].kwh[0], data.kwh[1] + kwhFindsWeek[0].kwh[1], data.kwh[2] + kwhFindsWeek[0].kwh[2]]
                }
                KwhWeekSensor.findOneAndUpdate({
                        'created_at': {
                            $gte: startWeek,
                            $lte: endWeek
                        },
                        building_name: req.params.building_name,
                        parent_name: req.params.parent_name,
                        channel_name: req.params.channel_name,
                        node_name: req.params.node_name
                    }, {
                        $set: {
                            'volt': volt,
                            'current': current,
                            'kwh': kwh,
    
                        }
                    },
    
                    function(err, result4) {
                        next()
                    });
            }
        });
    
    }catch(err) {
        console.log(err)
    }
   

}


 async function setKWHDay(req, res, next) {

    try {
        data = req.body;
    // console.log(data)
    data.building_name = req.params.building_name;
    data.parent_name = req.params.parent_name;
    data.channel_name = req.params.channel_name;
    data.node_name = req.params.node_name;


    var startDay = moment().startOf('day').format();
    var endDay = moment().endOf('day').format();

    KwhDaySensor.find({
        'created_at': {
            $gte: startDay,
            $lte: endDay
        },
        building_name: req.params.building_name,
        parent_name: req.params.parent_name,
        channel_name: req.params.channel_name,
        node_name: req.params.node_name
    },  await function(err, kwhFindsDay) {
        if (!kwhFindsDay || kwhFindsDay.length < 1) {
            let KwhDaySensorData = new KwhDaySensor(data);
            KwhDaySensorData.save(function(err, result) {
                if (err) {
                    res.status(400)
                } else {
                    next()
                }
            });
        } else {


            let volt = []
            let current = []
            let kwh = []
          
            if(data.kwh.length == 1) {
                kwh = [data.kwh[0] + kwhFindsDay[0].kwh[0]]
                volt = [data.volt[0]  ]
                current = [data.current[0] ]
                
            }else {
                volt = [data.volt[0] , data.volt[1] , data.volt[2] ]
                current = [data.current[0] , data.current[1],  data.current[2] ]
                kwh = [data.kwh[0] + kwhFindsDay[0].kwh[0], data.kwh[1] + kwhFindsDay[0].kwh[1], data.kwh[2] + kwhFindsDay[0].kwh[2]]
            }

            KwhDaySensor.findOneAndUpdate({
                    'created_at': {
                        $gte: startDay,
                        $lte: endDay
                    },
                    building_name: req.params.building_name,
                    parent_name: req.params.parent_name,
                    channel_name: req.params.channel_name,
                    node_name: req.params.node_name
                }, {
                    $set: {
                        'volt': volt,
                        'current': current,
                        'kwh': kwh,
                        'cost' : data.cost,


                    }
                },

                function(err, result3) {

                    next()
                });
        }
    });


        
    }catch(err) {
        console.log(err)
    }
    
}

async function setKWHHour(req, res, next) {

    try {
        data = req.body;
        data.building_name = req.params.building_name;
        data.parent_name = req.params.parent_name;
        data.channel_name = req.params.channel_name;
        data.node_name = req.params.node_name;
    
    
    
        var startHour = moment().startOf('hour').format();
        var endHour = moment().endOf('hour').format();
    
        KwhHourSensor.find({
            'created_at': {
                $gte: startHour,
                $lte: endHour
            },
            building_name: req.params.building_name,
            parent_name: req.params.parent_name,
            channel_name: req.params.channel_name,
            node_name: req.params.node_name
        },   await function(err, kwhFindsHour) {
    
            if (!kwhFindsHour || kwhFindsHour.length < 1) {
                let KwhHourSensorData = new KwhHourSensor(data);
                KwhHourSensorData.save( function(err, result) {
                    if (err) {
                        res.status(400);
                    } else {
                        next()
                    }
                });
            } else {
                let volt = []
                let current = []
                let kwh = []
              
                if(data.kwh.length == 1) {
                    kwh = [data.kwh[0] + kwhFindsHour[0].kwh[0]]
                    volt = [data.volt[0]  ]
                    current = [data.current[0] ]
                    power = [data.power[0]]
                    cosphi = [data.cosphi[0]]
                    frequency = [data.frequency[0]]
                    
                }else {
                    volt = [data.volt[0] , data.volt[1] , data.volt[2] ]
                    current = [data.current[0] , data.current[1],  data.current[2] ]
                    power = [data.power[0], data.power[1], data.power[2]]
                    cosphi = [data.cosphi[0], data.cosphi[1], data.cosphi[2]]
                    frequency = [data.frequency[0], data.frequency[1], data.frequency[2]]
                    kwh = [data.kwh[0] + kwhFindsHour[0].kwh[0], data.kwh[1] + kwhFindsHour[0].kwh[1], data.kwh[2] + kwhFindsHour[0].kwh[2]]
                }
                
                //kwhFinds[j].kwh.reduce((x, y) => x + y) 
                KwhHourSensor.findOneAndUpdate({
                        'created_at': {
                            $gte: startHour,
                            $lte: endHour
                        },
                        building_name: req.params.building_name,
                        parent_name: req.params.parent_name,
                        channel_name: req.params.channel_name,
                        node_name: req.params.node_name
                    }, {
                        $set: {
                            'volt': volt,
                            'current': current,
                            'kwh': kwh,
                            'power' : power,
                            'cosphi' : cosphi,
                            'frequency' : frequency,
                            'apparent_power' : data.apparent_power,
                            'Q' : data.Q,
                            'voltage_l_l' : data.voltage_l_l,
                            'power_3_phase' : data.power_3_phase,
                            'cost' : data.cost
                        }
                    },
    
                    function(err, result2) {
                        // console.log(result2)
                        next()
                    });
            }
        });
    
    

    }catch(err) {
        console.log(err)    
    }
   
}

 async function getKwh(req, res, next) {
    let data = req.body;

    data.building_name = req.params.building_name;
    data.parent_name = req.params.parent_name;
    data.channel_name = req.params.channel_name;
    data.node_name = req.params.node_name;

    let kwhSensor = new KwhSensor(data);
    let errData = kwhSensor.validateSync();

    var startDay = moment(req.params.start_date).startOf('day').format();
    var endDay = moment(req.params.end_date).endOf('day').format();
    if (res.statusCode == 200) {
        if (!errData) {
            KwhSensor.find({
                'created_at': {
                    $gte: startDay,
                    $lte: endDay
                },
                building_name: req.params.building_name,
                parent_name: req.params.parent_name,
                channel_name: req.params.channel_name,
                node_name: req.params.node_name
            }, await function(err, kwhFinds) {

                if (!kwhFinds || kwhFinds.length < 1) {
                    res.status(200).json({
                        pesan: "tidak ada data Monitoring",
                        data: kwhFinds,
                    });
                } else {

                    res.status(200).json({
                        pesan: "data Monitoring",
                        data: kwhFinds.map(data => {
                            return {
                                status: data.status,
                                volt: data.volt,
                                current: data.current,
                                kwh: data.kwh,
                                cost : data.cost,
                                power : data.power,
                                cosphi :data.cosphi,
                                frequency : data.frequency,
                                phase : data.phase,
                                apparent_power : data.apparent_power,
                                Q : data.Q,
                                voltage_l_l : data.voltage_l_l,
                                power_3_phase : data.power_3_phase,
                                created_at: data.created_at
                            }

                        })
                    });
                }
            });
        } else {
            res.status(200).json({
                pesan: "data error",
            });
        }
    } else {
        res.status(200).json({
            pesan: "Data Gedung TIdak tersedia",
        });
    }

}


async function getKwhMonth(req, res, next) {
    let data = req.body;

    data.building_name = req.params.building_name;
    data.parent_name = req.params.parent_name;
    data.channel_name = req.params.channel_name;
    data.node_name = req.params.node_name;

    let kwhSensor = new KwhMonthSensor(data);
    let errData = kwhSensor.validateSync();

    const startDay = moment(req.params.start_date).startOf('day').format();
    const endDay = moment(req.params.end_date).endOf('day').format();
    if (res.statusCode == 200) {
        if (!errData) {
            KwhMonthSensor.find({
                'created_at': {
                    $gte: startDay,
                    $lte: endDay
                },
                building_name: req.params.building_name,
                parent_name: req.params.parent_name,
                channel_name: req.params.channel_name,
                node_name: req.params.node_name
            }, await function(err, kwhFinds) {

                if (!kwhFinds || kwhFinds.length < 1) {
                    res.status(200).json({
                        pesan: "tidak ada data Monitoring",
                        data: kwhFinds,
                    });
                } else {

                    res.status(200).json({
                        pesan: "data Monitoring",
                        data: kwhFinds.map(data => {
                            return {
                                status: data.status,
                                volt: data.volt,
                                current: data.current,
                                kwh: data.kwh,
                                cost : data.cost,
                                created_at: data.created_at
                            }

                        })
                    });
                }
            });
        } else {
            res.status(200).json({
                pesan: "data error",
            });
        }
    } else {
        res.status(200).json({
            pesan: "Data Gedung TIdak tersedia",
        });
    }

}

async function getKwhWeek(req, res, next) {
    let data = req.body;

    data.building_name = req.params.building_name;
    data.parent_name = req.params.parent_name;
    data.channel_name = req.params.channel_name;
    data.node_name = req.params.node_name;

    let kwhSensor = new KwhWeekSensor(data);
    let errData = kwhSensor.validateSync();

    const startDay = moment(req.params.start_date).startOf('day').format();
    const endDay = moment(req.params.end_date).endOf('day').format();
    if (res.statusCode == 200) {
        if (!errData) {
            KwhWeekSensor.find({
                'created_at': {
                    $gte: startDay,
                    $lte: endDay
                },
                building_name: req.params.building_name,
                parent_name: req.params.parent_name,
                channel_name: req.params.channel_name,
                node_name: req.params.node_name
            }, await function(err, kwhFinds) {

                if (!kwhFinds ||  kwhFinds.length < 1) {
                    res.status(200).json({
                        pesan: "tidak ada data Monitoring",
                        data: kwhFinds,
                    });
                } else {

                    res.status(200).json({
                        pesan: "data Monitoring",
                        data: kwhFinds.map(data => {
                            return {
                                status: data.status,
                                volt: data.volt,
                                current: data.current,
                                kwh: data.kwh,
                                cost : data.cost,
                                created_at: data.created_at
                            }

                        })
                    });
                }
            });
        } else {
            res.status(200).json({
                pesan: "data error",
            });
        }
    } else {
        res.status(200).json({
            pesan: "Data Gedung TIdak tersedia",
        });
    }

}

async function getKwhDay(req, res, next) {
    let data = req.body;

    data.building_name = req.params.building_name;
    data.parent_name = req.params.parent_name;
    data.channel_name = req.params.channel_name;
    data.node_name = req.params.node_name;

    let kwhSensor = new KwhDaySensor(data);
    let errData = kwhSensor.validateSync();

    const startDay = moment(req.params.start_date).startOf('day').format();
    const endDay = moment(req.params.end_date).endOf('day').format();
    if (res.statusCode == 200) {
        if (!errData) {
            KwhDaySensor.find({
                'created_at': {
                    $gte: startDay,
                    $lte: endDay
                },
                building_name: req.params.building_name,
                parent_name: req.params.parent_name,
                channel_name: req.params.channel_name,
                node_name: req.params.node_name
            }, await function(err, kwhFinds) {

                if (!kwhFinds || kwhFinds.length < 1) {
                    res.status(200).json({
                        pesan: "tidak ada data Monitoring",
                        data: kwhFinds,
                    });
                } else {

                    res.status(200).json({
                        pesan: "data Monitoring",
                        data: kwhFinds.map(data => {
                            return {
                                status: data.status,
                                volt: data.volt,
                                current: data.current,
                                kwh: data.kwh,
                                cost : data.cost,
                                created_at: data.created_at
                            }

                        })
                    });
                }
            });
        } else {
            res.status(200).json({
                pesan: "data error",
            });
        }
    } else {
        res.status(200).json({
            pesan: "Data Gedung TIdak tersedia",
        });
    }

}

async function getKwhHour(req, res, next) {
    let data = req.body;

    data.building_name = req.params.building_name;
    data.parent_name = req.params.parent_name;
    data.channel_name = req.params.channel_name;
    data.node_name = req.params.node_name;

    let kwhSensor = new KwhHourSensor(data);
    let errData = kwhSensor.validateSync();

    const startDay = moment(req.params.start_date).startOf('day').format();
    // console.log(startDay)
    const endDay = moment(req.params.end_date).endOf('day').format();
    if (res.statusCode == 200) {
        if (!errData) {
            KwhHourSensor.find({
                'created_at': {
                    $gte: startDay,
                    $lte: endDay
                },
                building_name: req.params.building_name,
                parent_name: req.params.parent_name,
                channel_name: req.params.channel_name,
                node_name: req.params.node_name
            }, await function(err, kwhFinds) {

                if (!kwhFinds ||  kwhFinds.length < 1) {
                    res.status(200).json({
                        pesan: "tidak ada data Monitoring",
                        data: kwhFinds,
                    });
                } else {
                    
                    res.status(200).json({
                        pesan: "data Monitoring",
                        data: kwhFinds.map(data => {
                            return {
                                status: data.status,
                                volt: data.volt,
                                current: data.current,
                                kwh: data.kwh,
                                cost :  data.cost,
                                power : data.power,
                                cosphi : data.cosphi,
                                frequency : data.frequency,
                                apparent_power : data.apparent_power,
                                Q : data.Q,
                                voltage_l_l : data.voltage_l_l,
                                power_3_phase : data.power_3_phase,
                                created_at: data.created_at
                            }

                        })
                    });
                }
            });
        } else {
            res.status(200).json({
                pesan: "data error",
            });
        }
    } else {
        res.status(200).json({
            pesan: "Data Gedung TIdak tersedia",
        });
    }

}
async function getKwhMonthByGedung(req, res, next) {
    let data = req.body;

    data.building_name = req.params.building_name;

    let kwhSensor = new KwhMonthSensor(data);
    let errData = kwhSensor.validateSync();
    console.log(req.params.building_name)
    const startDay = moment(req.params.start_date).startOf('month').format();
   
    const endDay = moment(req.params.end_date).endOf('month').format();
    if (res.statusCode == 200) {
        if (!errData) {
            
            KwhDaySensor.find({
                'created_at': {
                    $gte: startDay,
                    $lte: endDay
                },
                building_name: req.params.building_name,
              
            }).sort({ 'created_at' : -1 } ).exec(
            await function(err, kwhFinds) {

                if (!kwhFinds || kwhFinds.length < 1) {
                    res.status(200).json({
                        pesan: "tidak ada data Monitoring",
                        data: kwhFinds,
                    });
                } else {
                    let total_kwh = [];
                    let selisih = moment(req.params.end_date).diff(moment(req.params.start_date), 'months');
                    // console.log(selisih)
                    
                    let add_day = moment(req.params.start_date)
                    // console.log(JSON.stringify(add_day))
             
                    for(var i = 0; i <= selisih; i++) {
                        
                        let total_temp = 0;
                        let total_cost = 0;
                        let created = '';
                        for (var j = 0 ; j <  kwhFinds.length ; j++) {
                            let now = moment(kwhFinds[j].created_at, ['YYYY/MM/DD'])
                            let tmp = moment(add_day, ['YYYY/MM/DD'])
                            let month_now = now.format('M');
                            let year_now = now.format('YYYY');
                            let month_tmp = tmp.format('M');
                            let year_tmp = tmp.format('YYYY');
                           
                            if( month_now == month_tmp && year_now == year_tmp) {
                                created = kwhFinds[j].created_at;
                                if(kwhFinds[j].kwh.length >= 1) {
                                    total_temp = kwhFinds[j].kwh.reduce((x, y) => x + y) + total_temp;
                                    if(kwhFinds[j].cost == undefined) {
                                        total_cost =  total_cost;
                                    }else {
                                        total_cost = kwhFinds[j].cost[0] + total_cost;
                                    }
                                    
                            
                                }
                              
                                
                            }
                             
                        }
                        
                        if (created == "") {
                            created = moment(add_day).add('1', 'months');
                        }
                        total_kwh.push({
                            kwh : total_temp,
                            cost : total_cost,
                            created_at :moment(created).format()
                        })
                        // console.log(total_temp)
                        add_day = moment(add_day).add('1', 'months');

                        
                        
                        
                        
                       
                    }
                    res.status(200).json({
                        pesan: "data Monitoring",
                        // data: kwhFinds,
                        total_kwh : total_kwh
                        
                    });
                }
            });
        } else {
            res.status(200).json({
                pesan: "data error",
            });
        }
    } else {
        res.status(200).json({
            pesan: "Data Gedung Tidak tersedia",
        });
    }

}

async function getKwhDayByGedung(req, res, next) {
    let data = req.body;

    data.building_name = req.params.building_name;

    let kwhSensor = new KwhHourSensor(data);
    let errData = kwhSensor.validateSync();
    console.log(req.params.building_name)
    const startDay = moment(req.params.start_date).startOf('day').format();
   
    const endDay = moment(req.params.end_date).endOf('day').format();
    if (res.statusCode == 200) {
        if (!errData) {
            
            KwhDaySensor.find({
                'created_at': {
                    $gte: startDay,
                    $lte: endDay
                },
                building_name: req.params.building_name,
              
            }).sort({ 'created_at' : -1 } ).exec(
            await function(err, kwhFinds) {

                if (!kwhFinds || kwhFinds.length < 1) {
                    res.status(200).json({
                        pesan: "tidak ada data Monitoring",
                        data: kwhFinds,
                    });
                } else {
                    let total_kwh = [];
                    let selisih = moment(req.params.end_date).diff(moment(req.params.start_date), 'days');
                    // console.log(selisih)
                    
                    let add_day = moment(req.params.start_date)
                    // console.log(JSON.stringify(add_day))
             
                    for(var i = 0; i <= selisih; i++) {
                        
                        let total_temp = 0;
                        let total_cost = 0;
                        let created = '';
                        for (var j = 0 ; j <  kwhFinds.length ; j++) {

                            let now = moment(kwhFinds[j].created_at, ['YYYY/MM/DD'])
                            let tmp = moment(add_day, ['YYYY/MM/DD'])
                            let day_now = now.format('D');
                            let month_now = now.format('M');
                            let year_now = now.format('YYYY');
                            let day_tmp = tmp.format('D');
                            let month_tmp = tmp.format('M');
                            let year_tmp = tmp.format('YYYY');
                           
                            
                            if(day_now == day_tmp && month_now == month_tmp && year_now == year_tmp) {
                                created = kwhFinds[j].created_at;
                                if(kwhFinds[j].kwh.length >= 1) {
                                    total_temp = kwhFinds[j].kwh.reduce((x, y) => x + y) + total_temp;
                                    if(kwhFinds[j].cost == undefined) {
                                        total_cost =  total_cost;
                                    }else {
                                        total_cost = kwhFinds[j].cost[0] + total_cost;
                                    }
                                    
                            
                                }
                              
                                
                            }
                             
                        }
                        
                        if (created == "") {
                            created = moment(add_day).add('1', 'days');
                        }
                        total_kwh.push({
                            kwh : total_temp,
                            cost : total_cost,
                            created_at :moment(created).format()
                        })
                        // console.log(total_temp)
                        add_day = moment(add_day).add('1', 'days');

                        
                        
                        
                        
                       
                    }
                    res.status(200).json({
                        pesan: "data Monitoring",
                        // data: kwhFinds,
                        total_kwh : total_kwh
                        
                    });
                }
            });
        } else {
            res.status(200).json({
                pesan: "data error",
            });
        }
    } else {
        res.status(200).json({
            pesan: "Data Gedung Tidak tersedia",
        });
    }

}

async function getKwhHourByGedung(req, res, next) {
    let data = req.body;

    data.building_name = req.params.building_name;

    let kwhSensor = new KwhHourSensor(data);
    let errData = kwhSensor.validateSync();
    console.log(req.params.building_name)
    const startDay = moment(req.params.start_date).startOf('day').format();
    // console.log(startDay)
    const endDay = moment(req.params.end_date).endOf('day').format();
    if (res.statusCode == 200) {
        if (!errData) {
            
            KwhHourSensor.find({
                'created_at': {
                    $gte: startDay,
                    $lte: endDay
                },
                building_name: req.params.building_name,
              
            }).sort({ 'created_at' : -1 } ).exec(
            await function(err, kwhFinds) {

                if (!kwhFinds || kwhFinds.length < 1) {
                    res.status(200).json({
                        pesan: "tidak ada data Monitoring",
                        data: kwhFinds,
                    });
                } else {
                    let total_kwh = [];
                    let selisih = moment(endDay, ['YYYY-MM-DD HH:ss']).diff(moment(startDay, ['YYYY-MM-DD HH:ss']), 'hours');
                    console.log(selisih)
                    
                    let add_day = moment(req.params.start_date)
             
                    for(var i = 0; i <= selisih; i++) {
                        
                        let total_temp = 0;
                        let total_cost = 0;
                        let created_at = '';
                        for (var j = 0; j  < kwhFinds.length; j++) {
                            if( moment(kwhFinds[j].created_at).hour() == i) {
                                created_at = kwhFinds[j].created_at;
                                if(kwhFinds[j].kwh.length >= 1) {
                                    total_temp = kwhFinds[j].kwh.reduce((x, y) => x + y) + total_temp;
                                    total_cost = kwhFinds[j].cost[0] + total_cost;
                                }else {
                                    j = kwhFinds.length;
                                }
                                
                            }
                             
                        }
                        if (created_at == "") {
                            created_at = moment(add_day).add('1', 'hours');
                        }
                        total_kwh.push({
                            kwh : total_temp,
                            cost : total_cost,
                            created_at : moment(created_at).format()
                        })
                       
                        add_day = moment(add_day).add('1', 'hours');
                        
                        
                        
                        
                       
                    }
                    res.status(200).json({
                        pesan: "data Monitoring",
                    
                        total_kwh : total_kwh
                        
                    });
                }
            });
        } else {
            res.status(200).json({
                pesan: "data error",
            });
        }
    } else {
        res.status(200).json({
            pesan: "Data Gedung Tidak tersedia",
        });
    }

}



async function getKwhMonthByDevice(req, res, next) {
    let data = req.body;

    data.building_name = req.params.building_name;
    data.node_name = req.params.node_name;

    let kwhSensor = new KwhMonthSensor(data);
    let errData = kwhSensor.validateSync();
    console.log(req.params.building_name)
    const startDay = moment(req.params.start_date).startOf('month').format();
   
    const endDay = moment(req.params.end_date).endOf('month').format();
    if (res.statusCode == 200) {
        if (!errData) {
            
            KwhDaySensor.find({
                'created_at': {
                    $gte: startDay,
                    $lte: endDay
                },
                building_name: req.params.building_name,
                node_name : req.params.node_name,
              
            }).sort({ 'created_at' : -1 } ).exec(
            await function(err, kwhFinds) {

                if (!kwhFinds || kwhFinds.length < 1) {
                    res.status(200).json({
                        pesan: "tidak ada data Monitoring",
                        data: kwhFinds,
                    });
                } else {
                    let total_kwh = [];
                    let selisih = moment(req.params.end_date).diff(moment(req.params.start_date), 'months');
                    // console.log(selisih)
                    
                    let add_day = moment(req.params.start_date)
                    // console.log(JSON.stringify(add_day))
             
                    for(var i = 0; i <= selisih; i++) {
                        
                        let total_temp = 0;
                        let total_cost = 0;
                        let created = '';
                        for (var j = 0 ; j <  kwhFinds.length ; j++) {
                            let now = moment(kwhFinds[j].created_at, ['YYYY/MM/DD'])
                            let tmp = moment(add_day, ['YYYY/MM/DD'])
                            let month_now = now.format('M');
                            let year_now = now.format('YYYY');
                            let month_tmp = tmp.format('M');
                            let year_tmp = tmp.format('YYYY');
                           
                            if( month_now == month_tmp && year_now == year_tmp) {
                                created = kwhFinds[j].created_at;
                                if(kwhFinds[j].kwh.length >= 1) {
                                    total_temp = kwhFinds[j].kwh.reduce((x, y) => x + y) + total_temp;
                                    if(kwhFinds[j].cost == undefined) {
                                        total_cost =  total_cost;
                                    }else {
                                        total_cost = kwhFinds[j].cost[0] + total_cost;
                                    }
                                    
                            
                                }
                              
                                
                            }
                             
                        }
                        
                        if (created == "") {
                            created = moment(add_day).add('1', 'months');
                        }
                        total_kwh.push({
                            kwh : total_temp,
                            cost : total_cost,
                            created_at :moment(created).format()
                        })
                        // console.log(total_temp)
                        add_day = moment(add_day).add('1', 'months');

                        
                        
                        
                        
                       
                    }
                    res.status(200).json({
                        pesan: "data Monitoring",
                        // data: kwhFinds,
                        total_kwh : total_kwh
                        
                    });
                }
            });
        } else {
            res.status(200).json({
                pesan: "data error",
            });
        }
    } else {
        res.status(200).json({
            pesan: "Data Gedung Tidak tersedia",
        });
    }

}

async function getKwhDayByDevice(req, res, next) {
    let data = req.body;

    data.building_name = req.params.building_name;
    data.node_name = req.params.node_name;

    let kwhSensor = new KwhHourSensor(data);
    let errData = kwhSensor.validateSync();
    console.log(req.params.building_name)
    const startDay = moment(req.params.start_date).startOf('day').format();
   
    const endDay = moment(req.params.end_date).endOf('day').format();
    if (res.statusCode == 200) {
        if (!errData) {
            
            KwhDaySensor.find({
                'created_at': {
                    $gte: startDay,
                    $lte: endDay
                },
                building_name: req.params.building_name,
                node_name : req.params.node_name,
              
            }).sort({ 'created_at' : -1 } ).exec(
            await function(err, kwhFinds) {

                if (!kwhFinds || kwhFinds.length < 1) {
                    res.status(200).json({
                        pesan: "tidak ada data Monitoring",
                        data: kwhFinds,
                    });
                } else {
                    let total_kwh = [];
                    let selisih = moment(req.params.end_date).diff(moment(req.params.start_date), 'days');
                    // console.log(selisih)
                    
                    let add_day = moment(req.params.start_date)
                    // console.log(JSON.stringify(add_day))
             
                    for(var i = 0; i <= selisih; i++) {
                        
                        let total_temp = 0;
                        let total_cost = 0;
                        let created = '';
                        for (var j = 0 ; j <  kwhFinds.length ; j++) {

                            let now = moment(kwhFinds[j].created_at, ['YYYY/MM/DD'])
                            let tmp = moment(add_day, ['YYYY/MM/DD'])
                            let day_now = now.format('D');
                            let month_now = now.format('M');
                            let year_now = now.format('YYYY');
                            let day_tmp = tmp.format('D');
                            let month_tmp = tmp.format('M');
                            let year_tmp = tmp.format('YYYY');
                           
                            
                            if(day_now == day_tmp && month_now == month_tmp && year_now == year_tmp) {
                                created = kwhFinds[j].created_at;
                                if(kwhFinds[j].kwh.length >= 1) {
                                    total_temp = kwhFinds[j].kwh.reduce((x, y) => x + y) + total_temp;
                                    if(kwhFinds[j].cost == undefined) {
                                        total_cost =  total_cost;
                                    }else {
                                        total_cost = kwhFinds[j].cost[0] + total_cost;
                                    }
                                    
                            
                                }
                              
                                
                            }
                             
                        }
                        
                        if (created == "") {
                            created = moment(add_day).add('1', 'days');
                        }
                        total_kwh.push({
                            kwh : total_temp,
                            cost : total_cost,
                            created_at :moment(created).format()
                        })
                        // console.log(total_temp)
                        add_day = moment(add_day).add('1', 'days');

                       
                    }
                    res.status(200).json({
                        pesan: "data Monitoring",
                        // data: kwhFinds,
                        total_kwh : total_kwh
                        
                    });
                }
            });
        } else {
            res.status(200).json({
                pesan: "data error",
            });
        }
    } else {
        res.status(200).json({
            pesan: "Data Gedung Tidak tersedia",
        });
    }

}
async function getKwhHourByDevice(req, res, next) {
    let data = req.body;

    data.building_name = req.params.building_name;
    data.node_name = req.params.node_name;

    let kwhSensor = new KwhHourSensor(data);
    let errData = kwhSensor.validateSync();
    console.log(req.params.building_name)
    const startDay = moment(req.params.start_date).startOf('day').format();
    // console.log(startDay)
    const endDay = moment(req.params.end_date).endOf('day').format();
    if (res.statusCode == 200) {
        if (!errData) {
            
            KwhHourSensor.find({
                'created_at': {
                    $gte: startDay,
                    $lte: endDay
                },
                building_name: req.params.building_name,
                node_name : req.params.node_name,
              
            }).sort({ 'created_at' : -1 } ).exec(
            await function(err, kwhFinds) {

                if (!kwhFinds || kwhFinds.length < 1) {
                    res.status(200).json({
                        pesan: "tidak ada data Monitoring",
                        data: kwhFinds,
                    });
                } else {
                    let total_kwh = [];
                    let selisih = moment(endDay, ['YYYY-MM-DD HH:ss']).diff(moment(startDay, ['YYYY-MM-DD HH:ss']), 'hours');
                    console.log(selisih)
                    
                    let add_day = moment(req.params.start_date)
                    let total_cost_all = 0;
                    for(var i = 0; i <= selisih; i++) {
                        
                        let total_temp = 0;
                        let total_cost = 0;
                        let volt = 0;
                        let current  = 0;
                        let power  = 0;
                        let cosphi  = 0;
                        let frequency  = 0;
                        let apparent_power  = 0;
                        let Q  = 0;
                        let voltage_l_l  = 0;
                        let reactive_power  = 0;
                        let power_3_phase  = 0;
                        
                        let created_at = '';
                        for (var j = 0; j  < kwhFinds.length; j++) {
                            if( moment(kwhFinds[j].created_at).hour() == i) {
                                created_at = kwhFinds[j].created_at;
                                if(kwhFinds[j].kwh.length >= 1) {
                                    total_temp = kwhFinds[j].kwh.reduce((x, y) => x + y) + total_temp;
                                    total_cost = kwhFinds[j].cost.reduce((x, y) => x + y) + total_cost;
                                    total_cost_all = total_cost_all + total_cost;
                                    volt = kwhFinds[j].volt ;
                                    current  = kwhFinds[j].current;
                                     power  = kwhFinds[j].power;
                                    cosphi  = kwhFinds[j].cosphi;
                                    frequency  = kwhFinds[j].frequency;
                                    apparent_power  = kwhFinds[j].apparent_power;
                                    Q  = kwhFinds[j].Q;
                                    voltage_l_l  = kwhFinds[j].voltage_l_l;
                                    reactive_power  = kwhFinds[j].reactive_power;
                                    power_3_phase  = kwhFinds[j].power_3_phase;

                                }else {
                                    j = kwhFinds.length;
                                }
                                
                            }
                             
                        }
                        if (created_at == "") {
                            created_at = moment(add_day).add('1', 'hours');
                        }
                        total_kwh.push({
                            kwh : total_temp,
                            cost : total_cost,
                            total_cost_all : total_cost_all,
                            volt : volt,
                            current  : current,
                            power  : power,
                            cosphi : cosphi,
                            frequency : frequency,
                            apparent_power  : apparent_power,
                            Q : Q,
                            voltage_l_l : voltage_l_l,
                            reactive_power  : reactive_power,
                            power_3_phase  : power_3_phase,
                            created_at : moment(created_at).format()
                        })
                       
                        add_day = moment(add_day).add('1', 'hours');
                        
                        
                        
                        
                       
                    }
                    res.status(200).json({
                        pesan: "data Monitoring",
                    
                        total_kwh : total_kwh
                        
                    });
                }
            });
        } else {
            res.status(200).json({
                pesan: "data error",
            });
        }
    } else {
        res.status(200).json({
            pesan: "Data Gedung Tidak tersedia",
        });
    }

}


exports.setKwh = setKwh;
exports.getKwh = getKwh;
exports.getKwhMonth = getKwhMonth;
exports.getKwhWeek = getKwhWeek;
exports.getKwhDay = getKwhDay;
exports.getKwhHour = getKwhHour;
exports.setKWHHour = setKWHHour;
exports.setKWHDay = setKWHDay;
exports.setKWHWeek = setKWHWeek;
exports.setKWHMonth = setKWHMonth;

exports.getKwhDayByGedung = getKwhDayByGedung;
exports.getKwhHourByGedung = getKwhHourByGedung;
exports.getKwhMonthByGedung = getKwhMonthByGedung;

exports.getKwhDayByDevice = getKwhDayByDevice;
exports.getKwhHourByDevice = getKwhHourByDevice;
exports.getKwhMonthByDevice = getKwhMonthByDevice;