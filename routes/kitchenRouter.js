var express = require('express');
var router = express.Router();

const kitchenController = require('./../controller/kitchenController');
const auth = require('./../middleware/authentication')

//BUAT POST DATA
router.post('/:building_name/:kitchen_type', kitchenController.saveKitchen, function(req, res, next) {});


//BUAT GET DATA
router.get('/:building_name/:kitchen_type', kitchenController.getKitchen, function(req, res, next) {});

//UPDATE STATUS
router.put('/:building_name/:kitchen_type', kitchenController.updateKitchen, function(req, res, next) {});


module.exports = router;