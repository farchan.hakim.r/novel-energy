const include = require('../routes/includeRouter/include');

const mongoose = include.mongoose;
const moment = include.moment;

const kitchenSchema = new mongoose.Schema({
    building_name: { type: String },
    kitchen_type: { type: String },
    updated_at: { type: String, default: moment().format() },
    detail_request: [{ // untuk data kirim
        _id: false,
        temperature: { type: Number },
        humidity: { type: Number },
        intensity: { type: Number },
        gas_total: { type: Number },
    }],
    detail_respone: [{ // untuk data terima
        _id: false,
        threshold_fire: { type: Number },
    }],
    detail_status: [{
        _id: false,
        status: [{ type: Boolean }]
    }]




})

const KitchenSchema = mongoose.model('Kitchen', kitchenSchema);

module.exports = KitchenSchema;