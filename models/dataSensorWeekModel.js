const include = require('../routes/includeRouter/include');

const mongoose = include.mongoose;
const moment = include.moment;

const sensorWeekSchema = new mongoose.Schema({
    building_name: { type: String },
    parent_name: { type: String },
    channel_name: { type: String },
    node_name: { type: String },
    status: { type: Boolean, default: true },
    volt: [{ type: Number }],
    current: [{ type: Number }],
    kwh: [{ type: Number }],
    created_at: { type: String, default: moment().format() },
    created_at_ISO: { type: Date, default: new Date },


})

const SensorWeekKwh = mongoose.model('SensorWeekKwh', sensorWeekSchema);

module.exports = SensorWeekKwh;