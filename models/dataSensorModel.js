const include = require('../routes/includeRouter/include');

const mongoose = include.mongoose;
const moment = include.moment;

const sensorSchema = new mongoose.Schema({
    building_name: { type: String },
    parent_name: { type: String },
    channel_name: { type: String },
    node_name: { type: String },
    status: { type: Boolean, default: true },
    volt: [{ type: Number }],
    current: [{ type: Number }],
    kwh: [{ type: Number }],
    power : [{ type: Number }],
    cosphi : [{type : Number}],
    frequency : [{ type: Number }],
    apparent_power : [{type : Number}],
    cost : [{type : Number, default : 0}],
    Q : [{type : Number}],
    voltage_l_l : [{type : Number}],
    reactive_power : [{type : Number}],
    power_3_phase : [{type : Number}],
    created_at: { type: String, default: moment().format() },
    created_at_ISO: { type: Date, default: new Date },


})

const SensorSchema = mongoose.model('SensorKwh', sensorSchema);

module.exports = SensorSchema;