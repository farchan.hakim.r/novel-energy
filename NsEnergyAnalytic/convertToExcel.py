# # Import pandas

import pandas as pd
from pandas import ExcelWriter
from pandas import ExcelFile
import numpy as np
# # Assign spreadsheet filename to `file`
excel_file = 'E:\\nsenergy\\hour.xlsx'
df = pd.read_excel(excel_file)

print("Column headings:")
print(df.columns)

kwh = []
created_at = []
b_name = []
p_name = []
c_name = []
n_name = []

for u in df['kwh']:
    kwh.append(u)
for i in df['created_at_ISO']:
    created_at.append(i)
for b in df['building_name']:
    b_name.append(b)
for p in df['parent_name']:
    p_name.append(p)
for c in df['channel_name']:
    c_name.append(c)
for n in df['node_name']:
    n_name.append(n)


        


df = pd.DataFrame({
        'building_name': b_name,
        'parent_name': p_name, 
        'channel_name': c_name, 
        'node_name': n_name,  
        'kwh' : kwh
        'created_at' : created_at    
    }
    )

writer = ExcelWriter('hour_fix.xlsx')
df.to_excel(writer,'Sheet1',index=False)
writer.save()