const include = require('./../routes/includeRouter/include');

const moment = include.moment;
const stringify = include.stringify;

const Pump = require('./../models/dataSensorPumpModel');
const PumpParameter = require('./../models/pumpModel');

const startDay = moment().startOf('day').format();
const endDay = moment().endOf('day').format();

const IndexSensor = {
    CURRENT: 0,
    VOLTAGE: 1,
    LEVEL: 3,
    DEBIT: 2,
    STATUS_PUMP_BY_HARDWARE: 4,
    ALERT: 5
}


function getPumpSensor(req, res, next) {
    const start = req.params.start_date;
    const end = req.params.start_date;
    let startDay;
    let endDay
    if (start != 1 && end != 1) {

        startDay = moment(start).startOf('day').format();
        endDay = moment(end).endOf('day').format();
    }
    if (res.statusCode == 200) {
        Pump.find({
            'detail.0.created_at': {
                $gte: startDay,
                $lte: endDay
            },
            building_name: req.params.building_name,
            pump_type: req.params.pump_type
        }, function(err, pumpFinds) {
            if (pumpFinds.length < 1) {
                res.status(404).json({
                    pesan: "data tidak ditemukan"
                })

            } else {
                const newarr = pumpFinds[0].detail.sort((a, b) => {
                    return moment(a.created_at).diff(b.created_at);
                });
                let dataFilter = [];

                if (start != 1 && end != 1) {
                    for (var i = 0; i < newarr.length; i++) {
                        if (newarr[i].created_at > startDay && newarr[i].created_at < endDay) {
                            dataFilter.push(newarr[i])
                        }
                    }
                    res.status(200).json({
                        data: dataFilter
                    })
                } else {
                    res.status(200).json({
                        data: newarr
                    })
                }

            }
        });

    } else {
        res.status(404).json({
            pesan: "token tidak berlaku",
        });
    }
}

function getPumpSensorStatus(req, res, next) {
    if (res.statusCode == 200) {
        Pump.find({ building_name: req.params.building_name, pump_type: req.params.pump_type }, function(err, pumpFinds) {
            if (pumpFinds.length < 1) {
                res.status(404).json({
                    pesan: "data tidak ditemukan"
                })

            } else {
                var status_hardware = [];
                var status_software;

                // if (pumpFinds[0].status == false) {
                //     status_software = "false";
                // } else {
                //     status_software = true;
                // }
                let momentNow = moment().format();
                Pump.findOneAndUpdate({ building_name: req.params.building_name, pump_type: req.params.pump_type }, {
                        $set: {
                            'updated_at': momentNow

                        }
                    },
                    function(err, result) {
                        res.status(200).json({
                            pesan: "status data sensor",
                            status_by_hardware: pumpFinds[0].detail[0].status_pump,
                            status_by_software: pumpFinds[0].status
                        });

                    });

            }
        });

    } else {
        res.status(404).json({
            pesan: "token tidak berlaku",
        });
    }
}

function savePumpSensor(req, res, next) {
    let data = req.body;
    data.building_name = req.params.building_name;
    data.pump_type = req.params.pump_type;
    //data.detail[0].created_at = moment().format();
    let pump = new Pump(data);
    let errData = pump.validateSync();

    if (res.statusCode == 200) {
        if (!errData) {
            Pump.find({ building_name: req.params.building_name, pump_type: req.params.pump_type }, function(err, pumpFinds) {
                if (pumpFinds.length < 1) {
                    pump.save(function(err, result) {
                        if (err) {
                            res.status(400).json({
                                pesan: "Gagal mengisi",
                            });
                        } else {
                            res.status(200).json({
                                pesan: "Berhasil mengisi data",
                                data: data,
                            });
                        }
                    });
                } else {
                    Pump.findOneAndUpdate({ building_name: req.params.building_name, pump_type: req.params.pump_type }, {
                            $push: {
                                'detail': {
                                    $each: data.detail,
                                    $position: 0,
                                }
                            }
                        },
                        function(err, result) {
                            res.status(200).json({
                                pesan: "tambah data ",
                                data: data,

                            });
                        });
                }
            });
        } else {
            res.status(404).json({
                pesan: "data error",
            });
        }
    } else {
        res.status(404).json({
            pesan: "token tidak berlaku",
        });
    }

}


function setStatusSensor(req, res, next) {
    let dataStatus = req.body;
    if (res.statusCode == 200) {
        Pump.find({ building_name: req.params.building_name, pump_type: req.params.pump_type }, function(err, pumpFinds) {
            if (pumpFinds.length < 1) {
                res.status(404).json({
                    pesan: "data building atau type pump tidak ditemukan"
                })
            } else {
                Pump.findOneAndUpdate({ building_name: req.params.building_name, pump_type: req.params.pump_type }, {
                        $set: {
                            'status': dataStatus.status

                        }
                    },
                    function(err, data) {
                        res.status(200).json({
                            pesan: "Ubah Status ",
                            data: dataStatus
                        });
                    });
            }
        });

    } else {
        res.status(404).json({
            pesan: "token tidak berlaku",
        });
    }

}

function getDataBySensor(req, res, next) {
    let index = req.params.index;
    let sensor_type;
    let recomendation_alert;
    let data_alert = [];
    let data_status_hardware = [];
    let status_pump_by_hardware;

    if (res.statusCode == 200) {
        Pump.find({ building_name: req.params.building_name, pump_type: req.params.pump_type }, function(err, pumpFinds) {
            if (pumpFinds.length < 1) {
                res.status(404).json({
                    pesan: "data tidak ditemukan"
                })

            } else {
                let data = pumpFinds[0].detail.map(data => {
                    let sensorData;
                    switch (parseInt(index)) {
                        case IndexSensor.CURRENT:
                            {
                                sensor_type = "Arus (I) ";
                                sensorData = data.current;
                                break;
                            }
                        case IndexSensor.VOLTAGE:
                            {
                                sensor_type = "Volt (V) ";
                                sensorData = data.volt;
                                break;
                            }
                        case IndexSensor.DEBIT:
                            {
                                sensor_type = "Debit (liter/menit) ";
                                sensorData = data.debit;

                                break;
                            }
                        case IndexSensor.LEVEL:
                            {
                                sensor_type = "Level (liter) ";
                                sensorData = data.level
                                break;
                            }

                    }
                    // console.log(data.created_at)
                    let time = data.created_at
                    return {
                        sensorValue: sensorData,
                        created_at: pumpFinds[0].updated_at
                    }
                });
                status_pump_by_hardware = pumpFinds[0].detail[0].status_pump;
                for (var i = 0; i < status_pump_by_hardware.length; i++) {
                    if (status_pump_by_hardware[i] == true) {
                        data_status_hardware[i] = 'Pump ke ' + i + ' aman';
                    } else {
                        data_status_hardware[i] = 'Pump ke ' + i + ' darurat';
                    }

                }

                recomendation_alert = pumpFinds[0].detail[0].alert;
                recomendation_alert = recomendation_alert.split('');

                for (var i = 0; i < 3; i++) {
                    if (recomendation_alert[i] == '1') {
                        data_alert.push('Arus transfer pump 1 terlalu tinggi');
                    } else if (recomendation_alert[i] == '2') {
                        data_alert.push('Arus transfer pump 1 terlalu rendah');
                    }
                }
                for (var i = 3; i < 6; i++) {
                    if (recomendation_alert[i] == '1') {
                        data_alert.push('Arus transfer pump 2 terlalu tinggi');
                    } else if (recomendation_alert[i] == '2') {
                        data_alert.push('Arus transfer pump 2 terlalu rendah');
                    }
                }
                for (var i = 6; i < 9; i++) {
                    if (recomendation_alert[i] == '1') {
                        data_alert.push('Tegangan transfer pump 1 terlalu rendah');
                    } else if (recomendation_alert[i] == '2') {
                        data_alert.push('Tegangan transfer pump 1 terlalu rendah');
                    }
                }
                for (var i = 9; i < 12; i++) {
                    if (recomendation_alert[i] == '1') {
                        data_alert.push('Tegangan transfer pump 2 terlalu tinggi');
                    } else if (recomendation_alert[i] == '2') {
                        data_alert.push('Tegangan transfer pump 2 terlalu rendah');
                    }
                }
                for (var i = 12; i < 14; i++) {
                    if (recomendation_alert[i] == '2') {
                        data_alert.push(req.params.pump_type + ' debit to LOW');
                    } else if (recomendation_alert[i] == '1') {
                        data_alert.push(req.params.pump_type + ' debit to HIGH');
                    }
                }
                for (var i = 14; i < 15; i++) {
                    if (recomendation_alert[i] == '1') {
                        data_alert.push(req.params.pump_type + ' level terlalu tinggi');
                    } else if (recomendation_alert[i] == '2') {
                        data_alert.push(req.params.pump_type + ' level terlalu rendah');
                    }
                }

                res.status(200).json({
                    pesan: "data arus sensor",
                    building_name: req.params.building_name,
                    pump_type: req.params.pump_type,
                    sensor_type: sensor_type,
                    data_alert: data_alert,
                    status_repair: pumpFinds[0].status_repair,
                    data_status_hardware: data_status_hardware,
                    data: data[0],
                })
            }
        });

    } else {
        res.status(404).json({
            pesan: "token tidak berlaku",
        });
    }
}


function getAllDataSensor(req, res, next) {
    let index = req.params.index;
    let sensor_type;
    let recomendation_alert;
    let data_alert = [];
    let data_status_hardware = [];
    let status_pump_by_hardware;
    if (res.statusCode == 200) {
        Pump.find({ building_name: req.params.building_name, pump_type: req.params.pump_type }, async function(err, pumpFinds) {
            if (pumpFinds.length < 1) {
                res.status(404).json({
                    pesan: "data tidak ditemukan"
                })

            } else {
                let data = await pumpFinds[0].detail.map(data => {

                    let time = data.created_at
                    return {
                        sensorValue: data,
                        created_at: pumpFinds[0].updated_at
                    }
                });
                status_pump_by_hardware = pumpFinds[0].detail[0].status_pump;
                for (var i = 0; i < status_pump_by_hardware.length; i++) {
                    if (status_pump_by_hardware[i] == true) {
                        data_status_hardware[i] = 'Pump ke ' + i + ' aman';
                    } else {
                        data_status_hardware[i] = 'Pump ke ' + i + ' darurat';
                    }

                }

                recomendation_alert = pumpFinds[0].detail[0].alert;
                recomendation_alert = recomendation_alert.split('');

                for (var i = 0; i < 3; i++) {
                    if (recomendation_alert[i] == '1') {
                        data_alert.push('Arus transfer pump 1 terlalu tinggi');
                    } else if (recomendation_alert[i] == '2') {
                        data_alert.push('Arus transfer pump 1 terlalu rendah');
                    }
                }
                for (var i = 3; i < 6; i++) {
                    if (recomendation_alert[i] == '1') {
                        data_alert.push('Arus transfer pump 2 terlalu tinggi');
                    } else if (recomendation_alert[i] == '2') {
                        data_alert.push('Arus transfer pump 2 terlalu rendah');
                    }
                }
                for (var i = 6; i < 9; i++) {
                    if (recomendation_alert[i] == '1') {
                        data_alert.push('Tegangan transfer pump 1 terlalu rendah');
                    } else if (recomendation_alert[i] == '2') {
                        data_alert.push('Tegangan transfer pump 1 terlalu rendah');
                    }
                }
                for (var i = 9; i < 12; i++) {
                    if (recomendation_alert[i] == '1') {
                        data_alert.push('Tegangan transfer pump 2 terlalu tinggi');
                    } else if (recomendation_alert[i] == '2') {
                        data_alert.push('Tegangan transfer pump 2 terlalu rendah');
                    }
                }
                for (var i = 12; i < 14; i++) {
                    if (recomendation_alert[i] == '2') {
                        data_alert.push(req.params.pump_type + ' debit to LOW');
                    } else if (recomendation_alert[i] == '1') {
                        data_alert.push(req.params.pump_type + ' debit to HIGH');
                    }
                }
                for (var i = 14; i < 15; i++) {
                    if (recomendation_alert[i] == '1') {
                        data_alert.push(req.params.pump_type + ' level terlalu tinggi');
                    } else if (recomendation_alert[i] == '2') {
                        data_alert.push(req.params.pump_type + ' level terlalu rendah');
                    }
                }

                res.status(200).json({
                    pesan: "data arus sensor",
                    building_name: req.params.building_name,
                    pump_type: req.params.pump_type,
                    sensor_type: sensor_type,
                    data_alert: data_alert,
                    status_repair: pumpFinds[0].status_repair,
                    data_status_hardware: data_status_hardware,
                    data: data[0]
                })
            }
        });
    } else {
        res.status(404).json({
            pesan: "token tidak berlaku",
        });
    }

}

function getGraphDataSensor(req, res, next) {
    let size_data = req.params.size_data;
    let counter = -1;
    if (res.statusCode == 200) {
        Pump.find({ building_name: req.params.building_name, pump_type: req.params.pump_type }, function(err, pumpFinds) {
            if (pumpFinds.length < 1) {
                res.status(404).json({
                    pesan: "data tidak ditemukan"
                })

            } else {
                PumpParameter.find({ building_name: req.params.building_name, pump_type: req.params.pump_type }, async function(err, parameterFinds) {
                    if (pumpFinds.length < 1) {
                        res.status(404).json({
                            pesan: "data tidak ditemukan"
                        })

                    } else {
                        let data = pumpFinds[0].detail.map(data => {
                            counter++;
                            if (counter <= size_data) {
                                return {
                                    counter: counter,
                                    current: data.current,
                                    voltage: data.volt,
                                    debit: data.debit,
                                    level: data.level,
                                    created_at: data.created_at
                                }
                            }

                        });


                        res.status(200).json({
                            pesan: "data graph arus sensor",
                            building_name: req.params.building_name,
                            pump_type: req.params.pump_type,
                            data: data,
                            parameter: parameterFinds[0].detail[0],
                            length: data.length,
                            last_update: pumpFinds[0].updated_at
                        })
                    }
                });


            }
        });
    } else {
        res.status(404).json({
            pesan: "token tidak berlaku",
        });
    }

}
exports.savePumpSensor = savePumpSensor;
exports.getPumpSensor = getPumpSensor;
exports.getDataBySensor = getDataBySensor;
exports.getAllDataSensor = getAllDataSensor;
exports.getGraphDataSensor = getGraphDataSensor;
exports.setStatusSensor = setStatusSensor;
exports.getPumpSensorStatus = getPumpSensorStatus;