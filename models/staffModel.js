const include = require('../routes/includeRouter/include');

const mongoose = include.mongoose;
const moment = include.moment;

const staffSchema = new mongoose.Schema({
    id: { type: String, unique: true, required: true },
    building_name: { type: String },
    detail: [{
        name: { type: String },
        pass: { type: String },
        email : {type : String},
        role: { type: Number },
        status : {type : Boolean, default : true},
        created_at: { type: String, default: moment().format() },
    }],
})

const Staff = mongoose.model('User', staffSchema);

module.exports = Staff;