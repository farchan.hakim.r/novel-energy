const include = require('./../routes/includeRouter/include');

const moment = include.moment;
const stringify = include.stringify;

const Kitchen = require('./../models/kitchenModel');

const startDay = moment().startOf('day').format();
const endDay = moment().endOf('day').format();



function getKitchen(req, res, next) {
    if (res.statusCode == 200) {
        Kitchen.find({ building_name: req.params.building_name, kitchen_type: req.params.kitchen_type }, function(err, kitchenFinds) {
            if (kitchenFinds.length < 1) {
                res.status(404).json({
                    pesan: "data tidak ditemukan"
                })

            } else {
                let data = {
                    detail_request: kitchenFinds[0].detail_request[0],
                    detail_respone: kitchenFinds[0].detail_respone[0],
                    detail_status: kitchenFinds[0].detail_status[0]
                }
                res.status(200).json({
                    pesan: "data sensor",
                    data: data
                })
            }
        });

    } else {
        res.status(404).json({
            pesan: "token tidak berlaku",
        });
    }
}



function saveKitchen(req, res, next) {
    let data = req.body;
    data.building_name = req.params.building_name;
    data.kitchen_type = req.params.kitchen_type;
    let kitchen = new Kitchen(data);
    let errData = kitchen.validateSync();

    if (res.statusCode == 200) {
        if (!errData) {
            Kitchen.find({ building_name: req.params.building_name, kitchen_type: req.params.kitchen_type }, function(err, kitchenFinds) {
                if (kitchenFinds.length < 1) {
                    kitchen.save(function(err, result) {
                        if (err) {
                            res.status(400).json({
                                pesan: "Gagal mengisi",
                            });
                        } else {
                            res.status(200).json({
                                pesan: "Berhasil mengisi data",
                                data: data,
                            });
                        }
                    });
                } else {
                    Kitchen.findOneAndUpdate({ building_name: req.params.building_name, kitchen_type: req.params.kitchen_type }, {
                            $push: {
                                'detail_request': {
                                    $each: data.detail_request,
                                    $position: 0,
                                    $slice: 1000
                                },
                                'detail_respone': {
                                    $each: data.detail_respone,
                                    $position: 0,
                                    $slice: 1000
                                },
                                'detail_status': {
                                    $each: data.detail_status,
                                    $position: 0,
                                    $slice: 1000
                                }
                            }
                        },
                        function(err, sukses) {
                            res.status(200).json({
                                pesan: "tambah data ",
                                data: data
                            });
                        });
                }
            });
        } else {
            res.status(404).json({
                pesan: "data error",
            });
        }
    } else {
        res.status(404).json({
            pesan: "token tidak berlaku",
        });
    }

}


function updateKitchen(req, res, next) {
    let data = req.body;
    data.building_name = req.params.building_name;
    data.kitchen_type = req.params.kitchen_type;
    let kitchen = new Kitchen(data);
    let errData = kitchen.validateSync();

    if (res.statusCode == 200) {
        if (!errData) {
            Kitchen.find({ building_name: req.params.building_name, kitchen_type: req.params.kitchen_type }, function(err, kitchenFinds) {
                if (kitchenFinds.length < 1) {
                    res.status(404).json({
                        pesan: "data tidak ditemukan"
                    });
                } else {
                    console.log(data.detail_respone)
                    if (data.detail_respone == undefined) {
                        data.detail_respone = []
                    }
                    Kitchen.findOneAndUpdate({ building_name: req.params.building_name, kitchen_type: req.params.kitchen_type }, {
                            $push: {
                                'detail_status': {
                                    $each: data.detail_status,
                                    $position: 0,
                                    $slice: 1000
                                },
                                'detail_respone': {
                                    $each: data.detail_respone,
                                    $position: 0,
                                    $slice: 1000
                                }
                            }
                        },
                        function(err, sukses) {
                            res.status(200).json({
                                pesan: "update data ",
                                data: data
                            });
                        });
                }
            });
        } else {
            res.status(404).json({
                pesan: errData,
            });
        }
    } else {
        res.status(404).json({
            pesan: "token tidak berlaku",
        });
    }

}

exports.saveKitchen = saveKitchen;
exports.getKitchen = getKitchen;;
exports.updateKitchen = updateKitchen;