var express = require('express');
var router = express.Router();

const sensorController = require('../controller/gedungController');
const auth = require('./../middleware/authentication')

//buat kirim data
router.post('/:building_name/:parent_name/:channel_name/:node_name', sensorController.setGedung, function(req, res, next) {});


router.get('/:building_name/:parent_name/:channel_name/:node_name/', sensorController.getGedung, function(req, res, next) {});

router.delete('/:building_name/:parent_name/:channel_name/:node_name/', sensorController.deleteGedung, function(req, res, next) {});


router.get('/list/:value', sensorController.allGedung, function(req, res, next) {});


router.get('/listDetailByNode/:value', sensorController.detailGedungByNode, function(req, res, next) {});


router.get('/distinct/', sensorController.distinctGedung, function(req, res, next) {});


router.get('/distinctNode/:building_name', sensorController.distinctNode, function(req, res, next) {});

router.get('/distinctFloor/:building_name', sensorController.distinctFloor, function(req, res, next) {});


router.post('/setPassword/:building_name/:parent_name/:channel_name/:node_name', sensorController.setPassword, function(req, res, next) {});

module.exports = router;