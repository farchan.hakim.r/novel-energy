const include = require('../routes/includeRouter/include');

const mongoose = include.mongoose;
const moment = include.moment;

const sensorSchema = new mongoose.Schema({
    building_name: { type: String },
 
    cost : [{type : Number}],
    cost_limit : [{type : Number}],
    volt: [{ type: Number }],
    current: [{ type: Number }],
    detail : [{type : String}],
    created_at: { type: String, default: moment().format() },
    created_at_ISO: { type: Date, default: new Date },

})

const SensorSchema = mongoose.model('paramSensor', sensorSchema);

module.exports = SensorSchema;