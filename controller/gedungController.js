const include = require('../routes/includeRouter/include');

const moment = include.moment;
const stringify = include.stringify;

const KwhSensor = require('../models/dataGedungModel');
const KwhHourSensor = require('../models/dataGedungModel');



function deleteGedung(req, res, next) {
    let data = req.body;

    data.building_name = req.params.building_name;
    data.parent_name = req.params.parent_name;
    data.channel_name = req.params.channel_name;
    data.node_name = req.params.node_name;

    let kwhSensor = new KwhSensor(data);
    let errData = kwhSensor.validateSync();


    if (res.statusCode == 200) {
        if (!errData) {
            KwhSensor.findOneAndDelete({
               
                building_name: req.params.building_name,
                parent_name: req.params.parent_name,
                channel_name: req.params.channel_name,
                node_name: req.params.node_name
            }, function(err, kwhFinds) {
                
                if (kwhFinds == null) {
                    res.status(404).json({
                        pesan: "tidak ada data ",
                        data: kwhFinds,
                    });
                } else {

                    res.status(200).json({
                        pesan: "data dihapus",
                        data: kwhFinds

                    });
                }
            });
        } else {
            res.status(404).json({
                pesan: "data error",
            });
        }
    } else {
        res.status(404).json({
            pesan: "token tidak berlaku",
        });
    }

}


function setGedung(req, res, next) {
    data = req.body;
    data.building_name = req.params.building_name;
    data.parent_name = req.params.parent_name;
    data.channel_name = req.params.channel_name;
    data.node_name = req.params.node_name;



    KwhHourSensor.find({
        
        node_name: req.params.node_name
    }, function(err, kwhFindsHour) {

        if (kwhFindsHour.length < 1) {
            let KwhHourSensorData = new KwhHourSensor(data);
            KwhHourSensorData.save(function(err, result) {
                if (err) {
                    res.status(400).json({
                        pesan: "Gagal mengisi",
                    });
                } else {
                    res.status(200).json({
                        pesan: "Berhasil mengisi data",
                        result
                    });
                }
            });
        } else {
            KwhHourSensor.findOneAndUpdate({
                
                node_name: req.params.node_name
            }, {
                $set: {
                    'detail': data.detail,
                    'building_name': req.params.building_name,
                    'parent_name': req.params.parent_name,
                    'channel_name': req.params.channel_name,
                }
            },

            function(err, result2) {
                res.status(200).json({
                    pesan: "Data berhasil update",
                    result2
                });
            });
            
        }
    });


}

function getGedung(req, res, next) {
    let data = req.body;

    data.building_name = req.params.building_name;
    data.parent_name = req.params.parent_name;
    data.channel_name = req.params.channel_name;
    data.node_name = req.params.node_name;

    let kwhSensor = new KwhSensor(data);
    let errData = kwhSensor.validateSync();


    if (res.statusCode == 200) {
        if (!errData) {
            KwhSensor.find({
               
                building_name: req.params.building_name,
                parent_name: req.params.parent_name,
                channel_name: req.params.channel_name,
                node_name: req.params.node_name
            }, function(err, kwhFinds) {

                if (kwhFinds.length < 1) {
                    res.status(404).json({
                        pesan: "tidak ada data ",
                        data: kwhFinds,
                    });
                } else {

                    res.status(200).json({
                        pesan: "data",
                        data: kwhFinds

                    });
                }
            });
        } else {
            res.status(404).json({
                pesan: "data error",
            });
        }
    } else {
        res.status(404).json({
            pesan: "token tidak berlaku",
        });
    }

}

function checkGedung(req, res, next) {
    let data = req.body;

    data.building_name = req.params.building_name;
    data.parent_name = req.params.parent_name;
    data.channel_name = req.params.channel_name;
    data.node_name = req.params.node_name;

    let kwhSensor = new KwhSensor(data);
    let errData = kwhSensor.validateSync();


    if (res.statusCode == 200) {
        if (!errData) {
            KwhSensor.find({
               
                building_name: req.params.building_name,
                parent_name: req.params.parent_name,
                channel_name: req.params.channel_name,
                node_name: req.params.node_name
            }, function(err, kwhFinds) {

                if (kwhFinds.length < 1) {
                    res.statusCode = 404;
                    next()
                } else {

                    res.statusCode  =200;
                    next()
                }
            });
        } else {
            res.status(404).json({
                pesan: "data error",
            });
        }
    } else {
        res.status(404).json({
            pesan: "token tidak berlaku",
        });
    }

}

function checkGedungForParam(req, res, next) {
    let data = req.body;

    data.building_name = req.params.building_name;
 

    let kwhSensor = new KwhSensor(data);
    let errData = kwhSensor.validateSync();


    if (res.statusCode == 200) {
        if (!errData) {
            KwhSensor.find({
               
                building_name: req.params.building_name,
          
            }, function(err, kwhFinds) {

                if (kwhFinds.length < 1) {
                    res.statusCode = 404;
                    next()
                } else {

                    res.statusCode  =200;
                    next()
                }
            });
        } else {
            res.status(404).json({
                pesan: "data error",
            });
        }
    } else {
        res.status(404).json({
            pesan: "token tidak berlaku",
        });
    }

}

function allGedung(req, res, next) {
    let val ;
    
    if (req.params.value  == "ALL") {
        val = {}
    }else {
        val = {"building_name" : req.params.value} 
    }
    //onsole.log(val)
    if (res.statusCode == 200) {
       
            KwhSensor.find( val  , function(err, kwhFinds) {

                if (kwhFinds.length < 1) {
                    res.status(404).json({
                        pesan: "tidak ada data ",
                        data: kwhFinds,
                    });
                } else {

                    res.status(200).json({
                        pesan: "data",
                        data: kwhFinds

                    });
                }
            });
        
    } else {
        res.status(404).json({
            pesan: "token tidak berlaku",
        });
    }

}


function detailGedungByNode(req, res, next) {
    let val ;
    
    if (req.params.value  == "ALL") {
        val = {}
    }else {
        val = {"node_name" : req.params.value} 
    }
    console.log(val)
    if (res.statusCode == 200) {
       
            KwhSensor.find( val  , function(err, kwhFinds) {

                if (kwhFinds.length < 1) {
                    res.status(404).json({
                        pesan: "tidak ada data ",
                        data: kwhFinds,
                    });
                } else {

                    res.status(200).json({
                        pesan: "data",
                        data: kwhFinds

                    });
                }
            });
        
    } else {
        res.status(404).json({
            pesan: "token tidak berlaku",
        });
    }

}

function distinctGedung(req, res, next) {
    let val ;

    //console.log(val)
    if (res.statusCode == 200) {
       
            KwhSensor.distinct( "building_name"  , function(err, kwhFinds) {

                if (kwhFinds.length < 1) {
                    res.status(404).json({
                        pesan: "tidak ada data ",
                        data: kwhFinds,
                    });
                } else {

                    res.status(200).json({
                        pesan: "data",
                        data: kwhFinds

                    });
                }
            });
        
    } else {
        res.status(404).json({
            pesan: "token tidak berlaku",
        });
    }

}



function distinctNode(req, res, next) {
    let val ;

    
    if (res.statusCode == 200) {
       
            KwhSensor.distinct("node_name", { building_name: req.params.building_name } , function(err, kwhFinds) {

                if (kwhFinds.length < 1) {
                    res.status(404).json({
                        pesan: "tidak ada data ",
                        data: kwhFinds,
                    });
                } else {

                    res.status(200).json({
                        pesan: "data",
                        data: kwhFinds

                    });
                }
            });
        
    } else {
        res.status(404).json({
            pesan: "token tidak berlaku",
        });
    }

}
function distinctFloor(req, res, next) {
    let val ;

    
    if (res.statusCode == 200) {
       
            KwhSensor.distinct("parent_name", { building_name: req.params.building_name } , function(err, kwhFinds) {

                if (kwhFinds.length < 1) {
                    res.status(404).json({
                        pesan: "tidak ada data ",
                        data: kwhFinds,
                    });
                } else {

                    res.status(200).json({
                        pesan: "data",
                        data: kwhFinds

                    });
                }
            });
        
    } else {
        res.status(404).json({
            pesan: "token tidak berlaku",
        });
    }

}

function checkGedungOne(req, res, next) {
    let data = req.body;

    data.building_name = req.params.building_name;

    let kwhSensor = new KwhSensor(data);
    let errData = kwhSensor.validateSync();


    if (res.statusCode == 200) {
        if (!errData) {
            KwhSensor.find({
               
                building_name: req.params.building_name,
            }, function(err, kwhFinds) {

                if (kwhFinds.length < 1) {
                    res.statusCode = 404;
                    next()
                } else {

                    res.statusCode  =200;
                    next()
                }
            });
        } else {
            res.status(404).json({
                pesan: "data error",
            });
        }
    } else {
        res.status(404).json({
            pesan: "token tidak berlaku",
        });
    }

}



function setPassword(req, res, next) {
    let data = req.body;
    data.building_name = req.params.building_name;
    data.parent_name = req.params.parent_name;
    data.channel_name = req.params.channel_name;
    data.node_name = req.params.node_name;

    let val = req.body;

    KwhHourSensor.find({
        building_name: req.params.building_name,
        parent_name: req.params.parent_name,
        channel_name: req.params.channel_name,
        node_name: req.params.node_name
    }, function(err, kwhFindsHour) {

        if (kwhFindsHour.length < 1) {
            let KwhHourSensorData = new KwhHourSensor(data);
            KwhHourSensorData.save(function(err, result) {
                if (err) {
                    res.status(400).json({
                        pesan: "Gagal mengisi",
                    });
                } else {
                    res.status(200).json({
                        pesan: "Berhasil mengisi data",
                        result
                    });
                }
            });
        } else {
            KwhHourSensor.findOneAndUpdate({
                building_name: req.params.building_name,
                parent_name: req.params.parent_name,
                channel_name: req.params.channel_name,
                node_name: req.params.node_name
            }, {
                $set: {
                    'detail': val.detail
                }
            },

            function(err, result2) {
                res.status(200).json({
                    pesan: "Data berhasil update",
                    data
                });
            });
            
        }
    });


}

exports.setGedung = setGedung;
exports.getGedung = getGedung;
exports.checkGedung = checkGedung;
exports.checkGedungOne = checkGedungOne;
exports.allGedung = allGedung;

exports.distinctGedung = distinctGedung;
exports.distinctNode = distinctNode;
exports.detailGedungByNode = detailGedungByNode;
exports.deleteGedung = deleteGedung;
exports.distinctFloor = distinctFloor; 

exports.setPassword = setPassword;
exports.checkGedungForParam = checkGedungForParam;