const include = require('../routes/includeRouter/include');

const mongoose = include.mongoose;
const moment = include.moment;

const sensorDaySchema = new mongoose.Schema({
    building_name: { type: String },
    parent_name: { type: String },
    channel_name: { type: String },
    node_name: { type: String },
    schedule : {type : String},
    user_name : {type : String},
    description : {type : String},
    status : {type : Boolean, default : true}, // TRUE : ACTIVE - FALSE : NON ACTIVE
    start_date : {type : String, default: moment().format()},
    end_date : {type : String, default: moment().format()},
    created_at: { type: String, default: moment().format() },
    created_at_ISO: { type: Date, default: new Date },


})

const SensorDayKwh = mongoose.model('schedule', sensorDaySchema);

module.exports = SensorDayKwh;