const include = require('./../routes/includeRouter/include');

const Staff = require('./../models/staffModel');
const config = require('../config');

const jwt = include.jwt;
const bcrypt = include.bcrypt;

function getLogin(req, res, next) {
    let data = req.body;
    console.log(data.pass)
    Staff.find({ id: data.id }, function(err, StaffFind) {
        if (StaffFind.length < 1) {
            res.status(200).json({
                pesan: "id tidak ditemukan",
                tipe: 0
            });
            // console.log(err)
        } else {
            Staff.find({ 'detail.0.pass': data.pass }, function(err, Pass) {
                if (Pass.length < 1) {
                    res.status(200).json({
                        pesan: "password tidak cocok",
                        tipe: 1
                    })
                    // console.log(err)
                } else {
                    let tokenStaff = ({
                        name: data.id,
                        password: data.pass,
                        // role: data.detail[0].role
                    });
                    jwt.sign({ tokenStaff }, config.secret, (err, token) => {
                        if (err) {
                            res.status(404).json({
                                pesan: 'Gagal',
                                tipe: 1
                            });
                            console.log(err)
                        } else {
                            res.status(200).json({
                                token: token,
                                tipe: 2,
                                detail : StaffFind[0]
                            });
                        }
                    });

                }
            });
        }
    });
}
// FOR STAFF REGISTRATION
function staffRegister(req, res, next) {
    let data = req.body;
    let staffData = new Staff(data);
    console.log(data)
    let errData = staffData.validateSync();
    if (!errData) {
        staffData.save(
            function(err, staff) {
                if (err) {
                    res.status(401).json({
                        pesan: "id sudah terdaftar sebelumnya",
                        tipe: 0,
                        err : err
                    });

                } else {
                    let tokenStaff = ({
                        name: data.id,
                        password: data.detail[0].pass,
                    });

                    jwt.sign({ tokenStaff }, config.secret, (err, token) => {
                        if (err) {
                            res.status(404).json({
                                pesan: 'Gagal',
                                tipe: 1
                            });
                            console.log(err)
                        } else {
                            res.status(200).json({
                                token: token,
                                tipe: 2
                            });
                        }
                    });
                }
            });
    } else {
        res.status(203).json({
            pesan: "Data harus diisi dengan benar",
        });
    }
}




exports.staffRegister = staffRegister;
exports.getLogin = getLogin;