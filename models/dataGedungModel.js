const include = require('../routes/includeRouter/include');

const mongoose = include.mongoose;
const moment = include.moment;

const sensorSchema = new mongoose.Schema({
    building_name: { type: String },
    parent_name: { type: String },
    channel_name: { type: String },
    node_name: { type: String },
    status: { type: Boolean, default: true },
    detail : {
        ssid :{type : String},
        pass : {type : String},
    },

    status : {type : Boolean, default : true},
    created_at: { type: String, default: moment().format() },
    created_at_ISO: { type: Date, default: new Date },


})

const SensorSchema = mongoose.model('dataGedung', sensorSchema);

module.exports = SensorSchema;