var express = require('express');
var router = express.Router();

const sensorController = require('../controller/scheduleController');
const gedungController = require('../controller/gedungController');
const auth = require('./../middleware/authentication')

//buat kirim data
router.post('/:building_name/:parent_name/:channel_name/:node_name/:start_date/:end_date', gedungController.checkGedung, sensorController.setGedung, function(req, res, next) {});


router.delete('/:building_name/:parent_name/:channel_name/:node_name/:start_date/:end_date', gedungController.checkGedung, sensorController.deleteGedung, function(req, res, next) {});

router.get('/:building_name/:parent_name/:channel_name/:node_name/:status', gedungController.checkGedung, sensorController.getGedung, function(req, res, next) {});

router.get('/:building_name/:parent_name/:channel_name/:node_name/:start_date/:end_date', gedungController.checkGedung, sensorController.getGedung, function(req, res, next) {});


router.get('/all/:building_name', sensorController.allGedung, function(req, res, next) {});



module.exports = router;