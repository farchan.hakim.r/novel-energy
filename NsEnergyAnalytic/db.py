import pymongo
import pprint
import pandas as pd
from pymongo import MongoClient
import datetime
from datetime import date, timedelta
from dateutil.relativedelta import relativedelta
from calendar import monthrange
import schedule 
import time 

client = MongoClient()
client = MongoClient('mongodb://localhost:27017/')

db = client['nusaenergy']
sensorkwhs = db['sensorkwhs']
sensorhourkwhs = db['sensorhourkwhs']

today = datetime.date.today()

def geeks(): 
    print("Waiting Scheduler") 

def getScheduleByFirstMonth():
    print("Convert to excel sensorkwhs") 
    getAllDataBeforeMonth()
    getDataSensorKWHhour()
    
def convertToExcel(data, title):
    df =  pd.DataFrame(list(data))
    print(df)
    df.to_csv( 'Excel/' + str(today) + title + '.csv', index=False)
 
def getRangeMonth():
    
    today = date.today()
    yesterday = today - timedelta(days = 7)
    # firstLastMonth = lastMonth.replace(day=1)
    print(yesterday)
    return  yesterday
def getMonthBefore(thresh):
    month = today.month
    year = today.year

    month_last = month - thresh
    if(month_last <= 0):
        month_last = month_last + 13
        year = year -1
    first = datetime.date(year, month_last, 1)
    lastMonth = first - datetime.timedelta(days=1)
    
    return lastMonth
    


def getDataSensorKWHhour():
    endDate = getMonthBefore(5)
  
    endDate = datetime.datetime(endDate.year, endDate.month, endDate.day)
    print(endDate)
    data = sensorhourkwhs.find(
        { 
            "created_at_ISO": {
                "$lte" : endDate
            } 
        })
    
    if(list(data) != []): 
        convertToExcel(data, '-sensorhourkwhs')
    else :
        print('[ DATA EMPYT ]')

    dataDelete = sensorhourkwhs.remove(
        { 
            "created_at_ISO": {
                "$lte" : endDate
            } 
        })
    print(dataDelete)

    
def getAllDataBeforeMonth():
    endDate = getRangeMonth()
  
    endDate = datetime.datetime(endDate.year, endDate.month, endDate.day)
    print(endDate)
    data = sensorkwhs.find(
        { 
            "created_at_ISO": {
                "$lte" : endDate
            } 
        })
    
    if(list(data) != []): 
        convertToExcel(data, '-sensorkwhs')
    else :
        print('[ DATA EMPYT ]')

    dataDelete = sensorkwhs.remove(
        { 
            "created_at_ISO": {
                "$lte" : endDate
            } 
        })
    print(dataDelete)


if __name__ == '__main__':
    
    # getMonthBefore(5)
    # getAllDataBeforeMonth()
   
    schedule.every().day.at("13:20").do(getScheduleByFirstMonth) 
    schedule.every(0.1).minutes.do(geeks) 
    while True: 
        schedule.run_pending() 
        time.sleep(1) 
        



    
