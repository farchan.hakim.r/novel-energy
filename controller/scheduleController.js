const include = require('../routes/includeRouter/include');

const moment = include.moment;
const stringify = include.stringify;

const KwhSensor = require('../models/scheduleModel');
const KwhHourSensor = require('../models/scheduleModel');






function setGedung(req, res, next) {
    data = req.body;
    data.building_name = req.params.building_name;
    data.parent_name = req.params.parent_name;
    data.channel_name = req.params.channel_name;
    data.node_name = req.params.node_name;

        
    
    data.start_date =  moment(req.params.start_date).format("YYYY-MM-DD HH:mm");
    data.end_date =  moment(req.params.end_date).format("YYYY-MM-DD HH:mm");
 
    if(res.statusCode == 200) {
        KwhHourSensor.find({
            building_name: req.params.building_name,
            parent_name: req.params.parent_name,
            channel_name: req.params.channel_name,
            node_name: req.params.node_name,
            status : true,
            start_date: { 
                    $lte: moment(req.params.start_date).format("YYYY-MM-DD HH:mm")
             
            },
            end_date: { 
              
                    $gte : moment(req.params.end_date).format("YYYY-MM-DD HH:mm")
              
            } 
        }, function(err, kwhFindsHour) {

            if (kwhFindsHour.length < 1) {
                let KwhHourSensorData = new KwhHourSensor(data);
                KwhHourSensorData.save(function(err, result) {
                    if (err) {
                        res.status(400).json({
                            pesan: "Gagal mengisi",
                        });
                    } else {
                        res.status(200).json({
                            pesan: "Berhasil mengisi data",
                            result
                        });
                    }
                });
            } else {
                res.status(400).json({
                    pesan: "data gedung sudah terisi",
                });
                
            }
        });
    }else {
        res.status(404).json({
            pesan: "Gedung tidak ada",
        });
    }


}

function getGedung(req, res, next) {
    let data = req.body;

    data.building_name = req.params.building_name;
    data.parent_name = req.params.parent_name;
    data.channel_name = req.params.channel_name;
    data.node_name = req.params.node_name;

    let kwhSensor = new KwhSensor(data);
    let errData = kwhSensor.validateSync();

    data.start_date =  moment(req.params.start_date).format("YYYY-MM-DD HH:mm");
    data.end_date =  moment(req.params.end_date).format("YYYY-MM-DD HH:mm");
 

    if (res.statusCode == 200) {
        if (!errData) {
            KwhSensor.find({
               
                building_name: req.params.building_name,
                parent_name: req.params.parent_name,
                channel_name: req.params.channel_name,
                node_name: req.params.node_name,
                start_date : data.start_date,
                end_date : data.end_date
            
            }, function(err, kwhFinds) {

                if (kwhFinds.length < 1) {
                    res.status(200).json({
                        pesan: "tidak ada data ",
                        data: kwhFinds,
                    });
                } else {

                    res.status(200).json({
                        pesan: "data",
                        data: kwhFinds

                    });
                }
            });
        } else {
            res.status(404).json({
                pesan: "data error",
            });
        }
    } else {
        res.status(404).json({
            pesan: "Gedung tidak ada",
        });
    }

}


function allGedung(req, res, next) {

    let dateNow = moment().format("YYYY-MM-DD HH:mm")
    console.log(dateNow)
    if (res.statusCode == 200) {
       
            KwhSensor.find( {
        
                building_name: req.params.building_name,
          
            }
             , function(err, kwhFinds) {

                if (kwhFinds.length < 1) {
                    res.status(200).json({
                        pesan: "tidak ada data ",
                        data: kwhFinds,
                    });
                } else {

                    res.status(200).json({
                        pesan: "data",
                        data: kwhFinds.map(data => {
                            if(dateNow > data.end_date ) {
                                data.status = false;
                            }else {
                                data.status = true;
                            }
                            return data
                        })

                    });
                }
            });
        
    } else {
        res.status(404).json({
            pesan: "Gedung tidak ada",
        });
    }

}

function deleteGedung(req, res, next) {
    let data = req.body;

    data.building_name = req.params.building_name;
    data.parent_name = req.params.parent_name;
    data.channel_name = req.params.channel_name;
    data.node_name = req.params.node_name;
    data.start_date =  moment(req.params.start_date).format("YYYY-MM-DD HH:mm");
    data.end_date =  moment(req.params.end_date).format("YYYY-MM-DD HH:mm");
    let kwhSensor = new KwhSensor(data);
    let errData = kwhSensor.validateSync();


    if (res.statusCode == 200) {
        if (!errData) {
            KwhSensor.findOneAndDelete({
               
                building_name: req.params.building_name,
                parent_name: req.params.parent_name,
                channel_name: req.params.channel_name,
                node_name: req.params.node_name,
                start_date : data.start_date,
                end_date : data.end_date
             
            }, function(err, kwhFinds) {

                if (kwhFinds == null) {
                    res.status(200).json({
                        pesan: "tidak ada data ",
                        data: kwhFinds,
                    });
                } else {

                    res.status(200).json({
                        pesan: "data sudah di hapus",
                        data: kwhFinds

                    });
                }
            });
        } else {
            res.status(404).json({
                pesan: "data error",
            });
        }
    } else {
        res.status(404).json({
            pesan: "Gedung tidak ada",
        });
    }

}



exports.setGedung = setGedung;
exports.getGedung = getGedung;
exports.allGedung = allGedung;
exports.deleteGedung = deleteGedung;
